﻿import Request from "../components/Request";

const requestType = "requestType";
const receiveType = "receiveType";

const saverequestType = "SaveRequest";
const savereceiveType = "SaveResponse";

const initialState = { requestResult: {}, isLoading: false };

export const actionCreators = {
  getRequest: id => async (dispatch, getState) => {
    if (id === getState().requests.id) {
      return;
    }

    dispatch({ type: requestType, id });
    var date = Date();
    // alert(id);
    // const url = `api/requests/GetRequestbyid/?id=${id}&date={date}`;
    const url = `http://10.10.18.13:6858/api/requests/GetRequestbyid/?id=${id}&date={date}`;
    const response = await fetch(url);
    const request = await response.json();
    dispatch({ type: receiveType, id, request });
  }
};
export const reducer = (state, action) => {
  state = state || initialState;
  // alert(JSON.stringify(action.type));
  if (action.type === requestType) {
    return {
      ...state,
      id: action.id,
      isLoading: true
    };
  }

  if (action.type === receiveType) {
    //alert("new"+JSON.stringify(action.request));
    var i = {
      ...state,
      id: action.id,
      open: false,
      requestResult: action.request,
      isLoading: false
    };
    return i;
  }
  return state;
};
