﻿import React from "react";
//import { Route } from 'react-router';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Layout, { HideNavLayout } from "./components/Layout";
//import Home from './components/Home';
import Counter from "./components/Counter";
import ReconFlow from "./components/rappidDesign/rappid";
import FetchData from "./components/FetchData";
import Request from "./components/Request";
import Login from "./components/login/Login";
import Group from "./components/Group";
import Rule from "./components/Rule";
import "./index.css";

export default props => (
  <div>
    <Router>
      <div>
        <Switch>
          <Route exact path="/" component={Login} />

          <Layout>
            <Route path="/counter" component={Counter} />
            <Route path="/fetchdata/:startDateIndex?" component={FetchData} />
            <Route path="/request/:id?" component={Request} />
            <Route
              path="/workflow/"
              component={
                ReconFlow
              } /*render={(props) => (<ReconFlow {...props} openProject={"e"} />)}*/
            />
            <Route path="/group" component={Group} />
            <Route path="/rule/" component={Rule} />
          </Layout>
        </Switch>
      </div>
    </Router>
  </div>
);
