﻿import React, { Component } from "react";
import { Link } from "react-router-dom";
// import { Glyphicon, Nav, Navbar, NavItem } from "react-bootstrap";
// import { LinkContainer } from "react-router-bootstrap";
import "./NavMenu.css";
import logo from "./assets/logo.png";
import { Button } from "antd";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { actionCreators } from "../store/WeatherForecasts";
import { Menu, Icon, Switch } from "antd";

const SubMenu = Menu.SubMenu;

class NavMenu extends Component {
  //export default props => (

  render() {
    // alert(JSON.stringify(this.props));
    return (
      // <Navbar inverse fixedTop fluid collapseOnSelect>
      //   <Navbar.Header>
      //     <Navbar.Brand>
      //       <div>
      //         <p style={{ fontSize: "20px", color: "white" }}>
      //           <img
      //             src={logo}
      //             style={{ width: 50, marginTop: "-20px", marginLeft: "-5px" }}
      //             alt="  Rule Engine"
      //           />
      //           Rule Engine
      //         </p>
      //       </div>
      //     </Navbar.Brand>
      //     <Navbar.Toggle />
      //   </Navbar.Header>
      //   <Navbar.Collapse>
      //     <Nav>
      //       <LinkContainer to={"/request/0"}>
      //         <NavItem>
      //           <Glyphicon glyph="plus" /> Create New
      //         </NavItem>
      //       </LinkContainer>
      //       <LinkContainer to={"/fetchdata"}>
      //         <NavItem>
      //           <Glyphicon glyph="th-list" /> Request Management
      //         </NavItem>
      //       </LinkContainer>
      //       {this.props.forecasts.map(request => (
      //         <LinkContainer key={request.id} to={`/request/${request.id}`}>
      //           <NavItem>{request.url}</NavItem>
      //         </LinkContainer>
      //       ))}
      //     </Nav>
      //   </Navbar.Collapse>
      // </Navbar>
      <div>
        <div className="logo">
          <Link to={"/fetchdata"}>
            <img
              src={logo}
              style={{ width: 80, padding: 10, display: "inline-block" }}
              alt="  Rule Engine"
            />
            <h3 style={{ display: "inline-block", marginTop: 10 }}>
              Rule Engine
            </h3>
          </Link>
        </div>
        <Menu
          className="menu-style"
          theme="dark"
          defaultOpenKeys={["0", "1", "2", "3", "4"]}
          mode="inline"
        >
          <Menu.Item key="0" style={{ fontSize: 17, fontWeight: "bold" }}>
            <Link to={"/request/0"}>
              <Icon
                type="plus-circle"
                style={{ fontSize: 17, fontWeight: "bold" }}
              />
              Create New
            </Link>
          </Menu.Item>
          {this.props.forecasts.map((request, index) => (
            <SubMenu
              key={index}
              title={
                <span>
                  <Icon style={{ fontSize: 17 }} type="api" />
                  <span style={{ fontWeight: "bold", fontSize: 17 }}>
                    {request.name}
                  </span>
                </span>
              }
            >
              {request.method === 1 ? (
                <Menu.Item key="1">
                  <Link to={`/request/${request.id}`}>
                    <span className="method post">POST</span>Create a new{" "}
                    {request.name}
                  </Link>
                </Menu.Item>
              ) : request.method === 2 ? (
                <Menu.Item key="2">
                  <Link to={`/request/${request.id}`}>
                    <span className="method put">PUT</span>Update a{" "}
                    {request.name}
                  </Link>
                </Menu.Item>
              ) : request.method === 3 ? (
                <Menu.Item key="3">
                  <Link to={`/request/${request.id}`}>
                    <span className="method get">GET</span>Get a list of{" "}
                    {request.name}
                  </Link>
                </Menu.Item>
              ) : request.method === 4 ? (
                <Menu.Item key="4">
                  <Link to={`/request/${request.id}`}>
                    <span className="method patch">PATCH</span>Update a{" "}
                    {request.name}
                  </Link>
                </Menu.Item>
              ) : (
                <Menu.Item key="5">
                  <Link to={`/request/${request.id}`}>
                    <span className="method get">GET</span>Delete a{" "}
                    {request.name}
                  </Link>
                </Menu.Item>
              )}
            </SubMenu>
          ))}
          <Menu.Item key="11" style={{ fontSize: 17, fontWeight: "bold" }}>
            <Link to={"/group"}>
              <Icon
                type="plus-circle"
                style={{ fontSize: 17, fontWeight: "bold" }}
              />
              Groups
            </Link>
          </Menu.Item>
          <Menu.Item key="11" style={{ fontSize: 17, fontWeight: "bold" }}>
            <Link to={"/rule"}>
              <Icon
                type="plus-circle"
                style={{ fontSize: 17, fontWeight: "bold" }}
              />
              Rules
            </Link>
          </Menu.Item>
          {/* <SubMenu
            key="sub1"
            title={
              <span>
                <Icon type="mail" />
                <span>Navigation One</span>
              </span>
            }
          >
            <Menu.Item key="1">Option 1</Menu.Item>
            <Menu.Item key="2">Option 2</Menu.Item>
            <Menu.Item key="3">Option 3</Menu.Item>
            <Menu.Item key="4">Option 4</Menu.Item>
          </SubMenu>
          <SubMenu
            key="sub2"
            title={
              <span>
                <Icon type="appstore" />
                <span>Navigtion Two</span>
              </span>
            }
          >
            <Menu.Item key="5">Option 5</Menu.Item>
            <Menu.Item key="6">Option 6</Menu.Item>
            <SubMenu key="sub3" title="Submenu">
              <Menu.Item key="7">Option 7</Menu.Item>
              <Menu.Item key="8">Option 8</Menu.Item>
            </SubMenu>
          </SubMenu>
          <SubMenu
            key="sub4"
            title={
              <span>
                <Icon type="setting" />
                <span>Navigation Three</span>
              </span>
            }
          >
            <Menu.Item key="9">Option 9</Menu.Item>
            <Menu.Item key="10">Option 10</Menu.Item>
            <Menu.Item key="11">Option 11</Menu.Item>
            <Menu.Item key="12">Option 12</Menu.Item>
          </SubMenu> */}
        </Menu>
      </div>
      //);
    );
  }
}

export default connect(
  state => state.weatherForecasts,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(NavMenu);
