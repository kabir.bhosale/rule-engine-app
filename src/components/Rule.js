﻿import React, { Component } from "react";
import { connect } from "react-redux";
import Chips, { Chip } from "react-chips";
import { Button, Form, Input, Select, Progress, Spin, Icon, Tag } from "antd";
const Option = Select.Option;
const FormItem = Form.Item;
const { TextArea } = Input;

var Modal_style = {
  position: "absolute",
  width: "300px",
  backgroundColor: "#fff",
  boxShadow: "5px",
  padding: "10px 40px",
  margin: "30px 20px 10px 550px",
  borderRadius: "2px"
};
var Modal_button_style = {
  margin: "10px",
  fontSize: "12px"
};
var Color_button_style = {
  margin: "10px",
  fontSize: "12px",
  background: "#1890ff",
  color: "white"
};
var Modal_icon_style = {
  margin: "-5px"
};
var Add_icon_style = {
  margin: "-10px",
  focusVisible: "false",
  //background: '#03a9f4',
  color: "#1890ff"
};

const Rule = Form.create()(
  class Rule extends Component {
    constructor(props) {
      super(props);
      this.state = {
        currentGroup: {},
        groupData: {},
        showModal: false,
        open: false,
        group: [],
        groupName1: []
      };
      this.saveRule = this.saveRule.bind(this);
      this.getGroup();
      this.getRule();
    }

    componentWillReceiveProps() {
      this.getGroup();
    }

    getRule() {
      fetch(`http://10.10.18.13:6858/api/Requests/GetRules`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(response => {
          response.json().then(body => {
            console.log("GET RULE :: ", body);
            //this.addGroupFormRef.resetFields();
            //this.handleGroupCancel();
            //this.getGroup();
          });
        })
        .catch(err => {
          console.log(err);
        });
    }

    saveRule() {
      this.props.form.validateFields((err, values) => {
        console.log("VALUES ::: ", values);

        var ids = "2,3,4"; //["2","3","4"];

        if (!err) {
          fetch(`http://10.10.18.13:6858/api/Requests/SaveRule`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json"
            },
            body: JSON.stringify({
              rule_name: values.ruleName,
              rule_desc: values.ruleDesc,
              group_ids: ids
            })
          })
            .then(response => {
              response.json().then(body => {
                console.log("BODY :: ", body);
                //this.addGroupFormRef.resetFields();
                //this.handleGroupCancel();
                //this.getGroup();
              });
            })
            .catch(err => {
              console.log(err);
            });
        }
      });
    }

    getGroup = () => {
      this.setState({
        groupName1: []
      });

      console.log("GET GROUP CALLED");

      fetch(`http://10.10.18.13:6858/api/Requests/GetGroups`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
          //   'Access-Control-Allow-Origin':'*'
        }
      })
        .then(response => {
          if (response == undefined) return {};
          return response.json();
        })
        .then(response => {
          console.log("GROUPS ::: ", response);
          // alert("Get groupData" + JSON.stringify(response));
          this.setState({ currentGroup: response });
          for (var i = 0; i < response.length; i++) {
            this.state.groupName1.push(response[i]["group_Name"]);
          }
          //  alert("Get groupData" + this.state.groupName1);
        })
        .catch(err => {
          // alert("Error");
        });
    };

    //handleValidate = (validator) => {
    //    alert("Validator" + JSON.stringify(validator));
    //    if (validator == undefined) {
    //        validator = {};
    //        //if (this.state.currentproperty.validations == undefined || this.state.currentproperty.validations == null) {
    //        //    this.state.currentproperty.validations = [];
    //        //}
    //        this.state.currentGroup.push(validator);
    //    }

    //    this.setState({ groupData: validator, group_Name: validator.group_Name, rule_desc: validator.rule_desc });
    //    this.setState({ showModal: true });
    //    this.setState({ open: true });
    //};

    onChange = e => {
      //alert(e.target.name);
      this.setState({ [e.target.name]: e.target.value });
      this.state.currentGroup[e.target.name] = e.target.value;
      // alert("groupData" + JSON.stringify(this.state.groupData));
    };

    onVChange = e => {
      //alert(e.target.name);
      this.setState({ [e.target.name]: e.target.value });
      this.state.groupData[e.target.name] = e.target.value;
      // alert("groupData" + JSON.stringify(this.state.groupData));
    };

    handleClose = () => {
      //alert(JSON.stringify(this.prop.requestResult));
    };

    onGroupChange = chips => {
      console.log("CHIP CHANGE :: ", chips);
      // alert("chips"+chips);
      this.setState({ group: chips });
      // alert("doka" + this.state.group);
      //  this.state.currentvalidation.group = chips;
      // alert(this.state.group);
      //alert(typeof (this.state.group));
    };
    //handleValidationSave = () => {

    //    //alert(this.state.rule);
    //    this.state.groupData.group_Name = this.state.group_Name;
    //    this.state.groupData.rule_desc = this.state.rule_desc;
    //    alert(JSON.stringify(this.state.groupData));

    //    this.setState({ open: false });

    //    fetch(`http://10.10.18.13:6858/api/Requests/SaveGroup`, {
    //        method: "POST",
    //        //  mode: 'no-cors',
    //        headers: {
    //            "Content-Type": "application/json"
    //            //   'Access-Control-Allow-Origin':'*'
    //        },
    //        body: JSON.stringify({
    //            "id": JSON.stringify(this.state.groupData.id),
    //            "Group_Name": JSON.stringify(this.state.groupData.group_Name),
    //            "rule_desc": JSON.stringify(this.state.groupData.rule_desc)
    //        })
    //    })
    //        .then(response => {
    //            console.log("saved successfully", response);
    //        })
    //        .catch(err => err);

    //};

    handleSave = () => {
      //  this.state.currentGroup.push(this.state.currentGroup);
      // alert(JSON.stringify(this.state.currentGroup));
      //fetch(`http://10.10.18.13:6858/api/Requests/SaveGroup`, {
      //    method: "POST",
      //    //  mode: 'no-cors',
      //    headers: {
      //        "Content-Type": "application/json"
      //        //   'Access-Control-Allow-Origin':'*'
      //    },
      //    body: JSON.stringify({
      //        "id": this.getUrlParameter("id"),
      //        "Group_Name": JSON.stringify(this.state.currentGroup.group_Name),
      //        "rule_desc": JSON.stringify(this.state.currentGroup.rule_desc)
      //    })
      //})
      //    .then(response => {
      //        console.log("saved successfully", response);
      //    })
      //    .catch(err => err);
    };

    //getUrlParameter(name) {
    //    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    //    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    //    var results = regex.exec(location.search);
    //    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    //};

    render() {
      const { form } = this.props;
      const { getFieldDecorator } = form;
      return (
        <div>
          <div>
            <Form layout="vertical" style={{ marginTop: "10%" }}>
              <FormItem label="Enter Rule Name">
                {getFieldDecorator("ruleName", {
                  rules: [{ required: true, message: "Please enter rule name" }]
                })(<Input />)}
              </FormItem>

              <FormItem label="Enter Rule Description">
                {getFieldDecorator("ruleDesc", {
                  rules: [
                    { required: true, message: "Please enter rule description" }
                  ]
                })(<TextArea />)}
              </FormItem>

              <FormItem label="Enter Group">
                {getFieldDecorator("groupName", {
                  rules: [{ required: true, message: "Please select group" }]
                })(
                  <Chips
                    value={this.state.group}
                    onChange={this.onGroupChange}
                    suggestions={this.state.groupName1}
                  />
                )}
              </FormItem>

              <Form.Item>
                <Button
                  style={{ marginLeft: "40%" }}
                  type="primary"
                  onClick={this.saveRule}
                >
                  Submit
                </Button>
                <Button style={{ marginLeft: "5%" }}>Cancel</Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      );
    }
  }
);
export default connect()(Rule);
