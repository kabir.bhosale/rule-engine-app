import React from 'react';
import { Modal,Table, Form,Icon,Select,Card} from 'antd';
import { store } from 'react-flow-diagram';
import CompactTable from './../../components/table/index';

const { Column, ColumnGroup } = Table;
const Option = Select.Option;
//const dataSource = [{
//  key: '1',
//  name: 'Valid Card',
//  age: 'Equals to 200',
//  address: '10 Downing Street'
//}, {
//  key: '2',
//  name: 'Valid Amount',
//  age: 'Less Than Equals to 100',
//  address: '10 Downing Street'
//}
//];

const columns = [{
  title: 'Validation Name',
  dataIndex: 'name',
  key: 'name',
},
{
  title: 'Rule',
  dataIndex: 'age',
  key: 'age',
},
{
  title: '',
  dataIndex: '',
  render: () =>
  <center>
    <div>
      <Icon className="icons" type="edit" />
      &nbsp;  <Icon className="icons" type="delete"  />
    </div></center>
}
];

const CSVDialogBox = Form.create()(
  class extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        isOpen: this.props.isOpen,
        nameTextValue: '',
        entityData: [{}],
        dataFields: [{}],
      };
      this.updateDataFields = this.updateDataFields.bind(this);
        
      }

      //geturlparameter(name) {
      //    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
      //    var regex = new regexp('[\\?&]' + name + '=([^&#]*)');
      //    var results = regex.exec(location.search);
      //    return results === null ? '' : decodeuricomponent(results[1].replace(/\+/g, ' '));
      //};

      //componentDidMount() {
      //    fetch(`api/Requests/GetWorkFlow/?id=${this.getUrlParameter("id")}`)
      //        .then(response => {
      //            if (response == undefined) return {};
      //            alert(response.json());
      //            return response.json()
      //        })
      //        .then((responseJson) => {

      //            if (JSON.stringify(responseJson) != "{}") {
      //                this.rappid.graph.fromJSON(responseJson);
      //            }
      //        })

      //}


    updateDataFields() {

      var configuredData = store.getState();
      console.log('Data', configuredData);
      //this.state.dataFields = configuredData.entity[0].custom.dataFields;
      //this.state.sectionDetailTypeText = configuredData.entity[0].custom.sectionDetailType;
    }



    render() {
      this.updateDataFields();
        //alert(JSON.stringify(this.props.dataSource));
      return (
        <div>
          <Modal className="customModal"

            title="Data Validation Rules"
            visible={this.props.isOpen}
            onOk={this.props.handleOk}
            onCancel={this.props.handleCancel}
            centered={true}
          >
            {/* <Input placeholder="Section Detail Types" value = {this.state.sectionDetailTypeText} onChange={this.handleOnChange} /> &nbsp;&nbsp;&nbsp; */}
            {/* <Table  dataSource={this.state.dataFields} columns={columns} scroll={ {y: 1300} } pagination = {false} /> */}
            <Card className="cardStyle">
            Section detail types
             <Select
                  showSearch
                  // placeholder="Card"
                  defaultValue="Data"
                  style={{width: "230px", marginLeft: "15px"}}
                >
                  <Option value="Card">Card</Option>
                  
                </Select><br/><br/>
            
            <CompactTable dataSource={this.props.dataSource} columns={columns} />
            </Card>
          </Modal>

        </div>
      )
    }
  });

export default CSVDialogBox;
