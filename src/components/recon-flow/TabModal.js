import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Button, Modal, Form, Input, Radio, Select, Progress, Spin, Icon } from 'antd';
import { Breadcrumb, Steps, Card, Row, Col } from 'antd';
import CustomTabs from './../../components/tabs/Tabs';
const Step = Steps.Step;
const Option = Select.Option;
/* const TabPane = Tabs.TabPane; */
const TabModal = Form.create()(
    class extends React.Component {
        constructor(props) {
            super(props);
            console.log(props);
            this.state = { visible: '', activeKey:this.props.activeTab, }
            this.showAddRecord = this.showAddRecord.bind(this);
            this.getCurrentTab = this.getCurrentTab.bind(this);
            this.setForm = this.setForm.bind(this);
            this.setData = this.setData.bind(this);

            console.log("TESTING CONST TABMODAL :: ",this.props.activeTab)
        }

        setData(key,value){
            this.props.setData(key,value)
        }

        getCurrentTab(value){
            this.props.setCurrentTab(value);    
        }

        showAddRecord(){
            this.props.onClick();
        }

        setForm(form){
            console.log("TAB MODAL PROPS :: ",this.props);
                console.log("TAB MODAL FORM :: ",form);
                //this.props.setForm(form);
        }
        
        componentWillReceiveProps(){
            console.log("MOUNT TAB MODAL :")

            this.setState({visible : this.props.visible})
        }

        callback = (key) => {
            console.log(key);
          }
        render() {
            const { visible, onCancel, onOk, onApply } = this.props;

            console.log("TESTING RENDER TABMODAL :: ",this.props.activeTab)
            
            return (
                <div>

                    
                    <Modal
                        visible={visible}
                        title="Source Function"
                        onOk={onOk}
                        onCancel={onCancel}
                        okText="Save"
                        className="customModal"
                        footer={[
                             <Button key="submit" type="primary" onClick={onApply}>
                             Apply
                             </Button>,
                            <Button key="back" type="primary" onClick={onOk}>OK</Button>,
                            <Button key="submit"  onClick={onCancel}>
                            Cancel
                            </Button>,
                           
                            ]}
                    >
                    <CustomTabs stepData = {this.props.stepData} setData = {this.setData}  setForm = {this.setForm} onClick = {this.showAddRecord} activeTab = {this.props.activeTab} getCurrentTab = {this.getCurrentTab}/>
                    </Modal>
                </div>
            );
        }
    }
)

export default TabModal;