import React, { Component } from 'react';
import { Form, Icon, Input, Button, Checkbox, Alert } from 'antd';
//import Dashboard from '../dashboard/DashboardLand';
import BackgroundImg from '../assets/login_background_img.svg';
import logo from '../assets/logo.png';
import { BrowserRouter as Router, Route, Link, Switch,Redirect } from 'react-router-dom';
//import logo1 from './../../assets/logos/logo-blue.png';
//import SelectRole from './../select-role/SelectRole'
import 'antd/dist/antd.css';
import '../../index.css';
import './Login.less';

//const urls = require('./../../utility/urls').default;
const FormItem = Form.Item;

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userName: "",
            password: "",
            validate: false,
            error_msg: false,
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, values) => {
            //if (!err) {
            //    //Please used below password & userName variable for POJO class
            //    console.log('Received values of form: ', values);
            //    console.log('Received values of form: ', values.password);
            //    console.log('Received values of form: ', values.userName);
            //    console.log("Object", values)

            //    fetch(urls.urlarray.Login, {
            //        mode: 'cors',
            //        headers: {
            //            'Accept': 'application/json',
            //            'Content-Type': 'application/json',
            //            'Access-Control-Allow-Origin': '*'
            //        },
            //        method: "POST",
            //        body: JSON.stringify({ userName: values.userName, password: values.password })
            //    })

            //        // POST method for login 
            //        // axios.post(apiBaseUrl + 'login', values)
            //        .then(function (response) {
            //            console.log("All Response - ", response);
            //            console.log(JSON.stringify(response));
            //            console.log("Response Status Code - ", response.status)

            //            if (response.status === 302) {
            //                console.log("Login successfull");
            //                this.setState({ validate: true });
            //                this.setState({ error_msg: false });
            //                console.log("After return", this.state.validate);
            //            }
            //            else if (response.status === 404) {
            //                console.log("Username password do not match");
            //                this.setState({ validate: false });
            //                this.setState({ error_msg: true });
            //            }
            //        }.bind(this))
            //        .catch(function (error) {
            //            console.log(error);
            //        });
            //}
            //alert(JSON.stringify(values));
            if (values.userName == 'rule.engine' && values.password == '12345') {
                this.setState({ validate: true });
            }
            else {
                this.setState({ validate: false });
            }
        });
        
       
        //alert(1);
    }

    render() {

        if(this.state.validate)
        {
            return <Redirect to="/fetchdata"/>
        }
        const { getFieldDecorator } = this.props.form;
       // const isValidated = this.state.validate;
        const isErrorMsg = this.state.error_msg;
        return (
            <div className="loginMaindiv">
                <div className="subDiv">
                    <div className="backgroundImg">
                        <img src={BackgroundImg} /> 
                    </div>

                    <div className="formdiv">
                        <Form onSubmit={this.handleSubmit} className="loginForm">
                            <div>
                                <img alt="logo" src={logo} className="optimusLogo" />
                                <h1 className="optimusTitle">Rule Engine</h1>
                            </div>
                            {/* {!isErrorMsg ? (<div></div>) : (<Alert style={{ marginTop: '46px' }} message={'Invalid username or password'} type="error" showIcon />)} */}

                            <FormItem className='loginFormItem'>
                                {!isErrorMsg ? (<div></div>) : (<Alert className="loginAlert" message={'Invalid username or password'} type="error" showIcon />)}

                                {getFieldDecorator('userName', {
                                    rules: [{ required: true, message: 'Please enter username!' }],
                                })(
                                    <Input prefix={<Icon className="loginIcons" type="user" />} placeholder=" Username" />
                                )}
                            </FormItem>
                            <FormItem>
                                {getFieldDecorator('password', {
                                    rules: [{ required: true, message: 'Please enter password!' }],
                                })(
                                    <Input prefix={<Icon className="loginIcons" type="lock" />} type="password" placeholder=" Password" />
                                )}
                            </FormItem>
                            <FormItem>
                                <div>
                                    <div className="loginCheckbox">
                                        {getFieldDecorator('remember', {
                                            valuePropName: 'checked',
                                            initialValue: true,
                                        })(
                                            <Checkbox>Remember me</Checkbox>
                                        )}
                                    </div>
                                    <div className="loginForgotPwd">
                                        <a href="#forgotPass">Forgot your password?</a>
                                    </div>
                                </div>
                            </FormItem>
                            <FormItem>
                                <Button type="primary" htmlType="submit" onClick={this.handleClick} className="loginButton">
                                    Login
                                    </Button>
                            </FormItem>
                        </Form>
                        <div className="bottomLogo">
                            {/*    <img alt="logo1" src={logo1} /> */}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const WrappedLoginForm = Form.create()(Login);

export default WrappedLoginForm;