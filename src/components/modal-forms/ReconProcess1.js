import React from 'react';
import 'antd/dist/antd.css';
import { Button, Modal, Form, Input, Radio, Select, Progress, Spin, Icon } from 'antd';
import { Breadcrumb, Steps, Card, Row, Col } from 'antd';
import CompactTable from './../../components/table/index';
import './../../styleSheet.less';
const Step = Steps.Step;
const Option = Select.Option;

const ReconProcess1 = Form.create()(
    class extends React.Component {
        constructor(props) {
            super(props);
            this.state = { visible: '' }

            this.columns = [
                 this.props.columns,
                 {
                     title: 'Paymentech',
                     dataIndex: 'Paymentech',
                     key: 'Paymentech'
                 },
                {
                  title: <div>
                    <Icon type="plus-circle" theme="filled" className="plusIcon" />
                  </div>,
                  dataIndex: 'Action',
                  key: 'Action',
                },
                                   
              ]; 
              
            this.columns1 = [
                {
                    title: "First Data",
                    dataIndex: "FirstData",
                    key: 'FirstData',

                },
                {
                    title: "Hello",
                    dataIndex: "textinput1",
                    key: 'textinput1',

                },
                {
                    title: <div>
                        <Icon type="plus-circle" theme="filled" className="plusIcon" />
                    </div>,
                    dataIndex: 'Action',
                    key: 'Action',
                },

            ];
            this.columns2 = [
                {
                    title: "Samson Billing",
                    dataIndex: "FirstData",
                    key: 'FirstData',

                },
                {
                    title: "",
                    dataIndex: "textinput1",
                    key: 'textinput1',

                },
                {
                    title: <div>
                        <Icon type="plus-circle" theme="filled" className="plusIcon" />
                    </div>,
                    dataIndex: 'Action',
                    key: 'Action',
                },

            ];
            this.state = {
                dataSource: [
                    {
                        Paymentech:
                          <Select placeholder="Transaction Type" className="selectElement" defaultValue="MOP">
                              <Option value="name" >MOP</Option>
                              <Option value="recordType">RecordType</Option>
                              <Option value="entityType">EntityType</Option>
                              <Option value="entityNo">EntityNo</Option>
                              <Option value="presentmentCurr">PresentmentCurr</Option>
                              <Option value="SubmissionDate">SubmissionDate</Option>
                              <Option value="PID">PID</Option>
                              <Option value="PIDShortName">PIDShortName</Option>
                              <Option value="SubmissionNo">SubmissionNo</Option>
                              <Option value="Record">Record#</Option>
                              <Option value="TxnDivision">TxnDivision#</Option>
                              <Option value="MerchantOrd">MerchantOrd#</Option>
                              <Option value="RDF1">RDF1#</Option>
                              <Option value="account">Account#</Option>
                              <Option value="Amount">Amount</Option>
                              <Option value="SettCurr">SettCurr</Option>
                              <Option value="ActionCode">ActionCode</Option>
                              <Option value="TransactionType">TransactionType</Option>
                              <Option value="AuthDateTime">AuthDateTime</Option>
                              <Option value="AuthCode">AuthCode</Option>
                              <Option value="AuthRespCode">AuthRespCode</Option>
                              <Option value="VendorRespCode">VendorRespCode</Option>
                              <Option value="CardSecValResp">CardSecValResp</Option>
                              <Option value="Trace">Trace#</Option>
                              <Option value="MCC">MCC</Option>
                          </Select>
            
                      ,
                      Samson:
                          <Select placeholder="--Select One--" className="selectElement">
                              <Option value="BAN">BAN</Option>
                              <Option value="ENT_SEQ_NO">ENT_SEQ_NO</Option>
                              <Option value="SYS_CREATION_DATE">SYS_CREATION_DATE</Option>
                              <Option value="OPERATOR_ID">OPERATOR_ID</Option>
                              <Option value="APPLICATION_ID">APPLICATION_ID</Option>
                              <Option value="DL_SERVICE_CODE">DL_SERVICE_CODE</Option>
                              <Option value="DEPOSIT_DATE">DEPOSIT_DATE</Option>
                              <Option value="DEPOSIT_BANK_CODE">DEPOSIT_BANK_CODE</Option>
                              <Option value="PYM_METHOD">PYM_METHOD</Option>
                              <Option value="PYM_SUB_METHOD">PYM_SUB_METHOD</Option>
                              <Option value="BANK_CODE">BANK_CODE</Option>
                              <Option value="BANK_ACCOUNT_NO">BANK_ACCOUNT_NO</Option>
                              <Option value="CHECK_NO">CHECK_NO</Option>
                              <Option value="CR_CARD_NO">CR_CARD_NO</Option>
                              <Option value="ORIGINAL_AMT">ORIGINAL_AMT</Option>
                              <Option value="ORIGINAL_BAN">ORIGINAL_BAN</Option>
                              <Option value="AMT_DUE">AMT_DUE</Option>
                              <Option value="SOURCE_TYPE">SOURCE_TYPE</Option>
                              <Option value="SOURCE_ID">SOURCE_ID</Option>
                              <Option value="FILE_SEQ_NO">FILE_SEQ_NO</Option>
                              <Option value="BATCH_NO">BATCH_NO</Option>
                              <Option value="BATCH_LINE_NO">BATCH_LINE_NO</Option>
                              <Option value="RECEIPT_ID">RECEIPT_ID</Option>
                              <Option value="RMS_REF_STORE_ID">RMS_REF_STORE_ID</Option>
                              <Option value="ACTV_CODE">ACTV_CODE</Option>
                              <Option value="ACTV_REASON_CODE">ACTV_REASON_CODE</Option>
                              <Option value="ACTV_DATE">ACTV_DATE</Option>
                              <Option value="ACTV_AMT">ACTV_AMT</Option>
                              <Option value="INTERNAL_ACTV_AMT">INTERNAL_ACTV_AMT</Option>
                          </Select>,
                      textinput1:
                        <Input size="small" value= '"VI"'   className="inputSmall" />
            
                      ,
                      textinput2:
                        <Input size="small"  className="inputSmall" />
            
                      ,
                      Condition: 'Jim Green',
                      Billing: '1111',
            
                      Action: <div><center>
                        <Icon className="icons" type="delete"/></center>
                      </div>
            
                    },
            
            
            
                  ],
                dataSource1: [
                    {
                        FirstData:
                            <Select placeholder="Transaction Type" className="inputSmall">
                                <Option value="name">Customer Name</Option>
                                <Option value="account">Customer Account</Option>
                                <Option value="transID">Transaction ID</Option>
                                <Option value="transAmt">Transaction Amount</Option>
                                <Option value="transRef">Transaction Ref#</Option>
                            </Select>

                        ,
                        textinput1:
                            <Input size="small" className="inputSmall" />

                        ,
                        Condition: 'Jim Green',
                        Billing: '1111',

                        Action: <div>
                            <center><Icon className="icons" type="delete" /></center>
                        </div>

                    },


                ],

                dataSource2: [
                    {
                        FirstData:
                            <Select placeholder="Record Type">
                                <Option value="name">Customer Name</Option>
                                <Option value="account">Customer Account</Option>
                                <Option value="transID">Transaction ID</Option>
                                <Option value="transAmt">Transaction Amount</Option>
                                <Option value="transRef">Transaction Ref#</Option>
                            </Select>

                        ,
                        textinput1:
                            <Input size="small"   className="inputSmall" />

                        ,
                        Condition: 'Jim Green',
                        Billing: '1111',
                        textinput2:
                            <Input size="small" placeholder="small size" />

                        ,
                        Action: <div>
                            <center><Icon className="icons" type="delete" /></center>
                        </div>

                    },

                ]
            }

        }

        render() {
            const { visible, onCancel, onOk } = this.props;
            // console.log("header11111",this.props.header1);


            return (
                <div>
                    <Modal
                        visible={visible}
                        title="Recon Function"
                        onOk={onOk}
                        okText="Next"
                        onCancel={onCancel}
                        className="customModal"
                    >

                        <Breadcrumb className="reconBreadcumb">
                            <Breadcrumb.Item className="processSteps">
                                <Steps size="small" current={0} progressDot>
                                    <Step title="Transaction Selection Criteria" />
                                    <Step title="Matching Rules" />
                                    <Step title="Reconcilation Summary"/>

                                </Steps>
                            </Breadcrumb.Item>
                        </Breadcrumb>

                        <br />

                        <p className="formLabels">Transaction Selection Criteria :</p>
                        <Row>
                            <Col span={24}>
                                <Card className="cardStyle">
                                <CompactTable columns={this.props.columns} dataSource={this.state.dataSource}/>
                                    {/* <CompactTable columns={this.columns1} dataSource={this.state.dataSource1} /> */}
                                </Card>
                            </Col>
                            {/* <Col span={12}>
                                <Card className="transactionTable">
                                    <CompactTable columns={this.columns2} dataSource={this.state.dataSource2} />
                                </Card>
                            </Col> */}
                        </Row>
                    </Modal>
                </div>
            );
        }
    }
)

export default ReconProcess1;