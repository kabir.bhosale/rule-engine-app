import React, { Component } from 'react';

import { Button, Steps, Breadcrumb, Radio, message, Modal, Row, Col, Card, Input, Image, Select, Form, Checkbox, Icon } from 'antd';

import './../../styleSheet.less';
import './SourceLoader.less'

import CompactTable from './../../components/table/index';



const Option = Select.Option;
const FormItem = Form.Item;

const RadioGroup = Radio.Group;

const SourceLoader = Form.create()(
  class extends React.Component {


    constructor(props) {
      super(props);
      this.state = { visible: '' }

      // This binding is necessary to make `this` work in the callback
  //    this.handleClick = this.handleClick.bind(this);
      //----------------------------------------------
      this.columns = [
        {
          title: "Field Name",
          dataIndex: "field",
          key: 'field',

        },
        {
          title: "Value",
          dataIndex: "value",
          key: 'value',

        },
        {
          title: "Type",
          dataIndex: "type",
          key: 'type',

        },
        {
          title: "Primary Key",
          dataIndex: "primary",
          key: 'primary'
        },
        {
          title: <div>
            <center><Icon type="plus-circle" theme="filled" className="plusIcon" /></center>
          </div>,
          dataIndex: 'Action',
          key: 'Action',
        }
      ];

      this.state = {
        dataSource: [
          {
            field: <Input size="small" placeholder="Field Name" className="inputSmall"/>
            ,
            value: <Input size="small" placeholder="Value"  className="inputSmall"/>
            ,
            type:
            <Select className="selectElement" placeholder="Datatype">
              <Option value="varchar">String</Option>
              <Option value="int">32-bit Integer</Option>
              <Option value="int64">64-bit Integer</Option>
              <Option value="double">Double</Option>
            </Select>
            ,

            primary: <center><Checkbox /></center>,

            Action: <div>
              <center><Icon className="icons" type="delete" /></center>
            </div>,

          },

          {
            field: <Input size="small" placeholder="Field Name"  className="inputSmall"/>
            ,
            value: <Input size="small" placeholder="Value"  className="inputSmall"/>
            ,
            type:<Select className="selectElement" placeholder="Datatype">
              <Option value="varchar">String</Option>
              <Option value="int">32-bit Integer</Option>
              <Option value="int64">64-bit Integer</Option>
              <Option value="double">Double</Option>
            </Select>
            ,

            primary: <center><Checkbox /></center>,

            Action: <div>
              <center><Icon className="icons" type="delete" /></center>
            </div>,
          },

        ]
      }



    }


  


    

    // handleOk = (e) => {
    //   console.log(e);
    //   this.setState({
    //     visible: false,
    //   });
    // }

    handleCancel = (e) => {
      console.log(e);
      this.setState({
        visible: false,
      });
    }

    handleOk = (e) => {
      
      e.preventDefault();
      this.props.form.validateFields((err) => {
      if(!err){
     
      this.setState({ next:true }); 
      }
      });
      }; 



   

    render() {
      const { getFieldDecorator } = this.props.form;
      const { visible, onCancel, onOk } = this.props;
    
      return (
        <div>

          <div>
           
            <Modal
              title="DB Writer"
              visible={visible}
              onOk={onOk}
              onCancel={onCancel}
              className="customModal">

              <Card className="cardStyle">
                <Form layout="vertical">
                  <Row>
                    <Col span={5}>
                      <FormItem label="Display Name:"/>
                    </Col>
                    <Col span={6}>
                    <Input className="inputNumber" onChange={this.handleOk}></Input>
                    </Col>

                    <Col span={5}>
                      <FormItem label="Database Type:"/>
                    </Col>
                    <Col span={6}>
                      <Select className="selectDatabase" >
                        <Option value="mongo" onChange={this.handleOk}>Mongo DB</Option>
                      </Select>
                    </Col>
                  </Row>

                  <Row>
                    <Col span={5}>
                      <FormItem label="Database Source:" />
                    </Col>
                    <Col span={6}>
                      <Input className="inputNumber" onChange={this.handleOk}></Input>
                    </Col>

                    <Col span={5}>
                      <FormItem label="Collection Name:"/> 
                     </Col>
                    <Col span={6}>
                      <Input className="inputNumber" onChange={this.handleOk}></Input>

                    </Col>
                  </Row>


                  <FormItem >

                    <Checkbox>Create Collection if not</Checkbox>

                  </FormItem>

                  <FormItem >

                    <RadioGroup name="radiogroup" defaultValue={1}>
                      <Radio value={1}>Insert</Radio>
                      <Radio value={2}>Update</Radio>
                    </RadioGroup>


                  </FormItem>
                </Form>
                <CompactTable columns={this.columns} dataSource={this.state.dataSource} />
            
              </Card>
            </Modal>
          </div>
        </div>
      )
    }
  }
)
export default SourceLoader;
