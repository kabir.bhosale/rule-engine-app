import React from "react";
import "antd/dist/antd.css";
import { Modal, Form, Input, Select, Progress, Spin, Icon } from "antd";

const Option = Select.Option;
const FormItem = Form.Item;
const { TextArea } = Input;

class Progressing extends React.Component {
  render() {
    return (
      <div>
        <center>
          <Spin size="large" tip="Processing ..." />
        </center>
      </div>
    );
  }
}

const AddNewGroup = Form.create()(
  class extends React.Component {
    constructor(props) {
      super(props);
      this.showSize = false;
      this.fieldTypeOptions = "";
    }

    onCancel() {}

    render() {
      const { visible, onCancel, onCreate, form } = this.props;
      const { getFieldDecorator } = form;

      return (
        <div>
          <Modal
            visible={visible}
            title="Add New Group"
            okText="Add"
            onCancel={onCancel}
            onOk={onCreate}
            className="verticalModal"
          >
            <Form layout="vertical">
              <FormItem label="Enter Group Name">
                {getFieldDecorator("groupName", {
                  rules: [
                    { required: true, message: "Please enter group name" }
                  ]
                })(<Input />)}
              </FormItem>

              <FormItem label="Enter Group Description">
                {getFieldDecorator("groupDesc", {
                  rules: [
                    {
                      required: true,
                      message: "Please enter group description Name!"
                    }
                  ]
                })(<TextArea />)}
              </FormItem>
            </Form>
          </Modal>
        </div>
      );
    }
  }
);
export default AddNewGroup;
