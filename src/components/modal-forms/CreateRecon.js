import React from 'react';
import 'antd/dist/antd.css';
import { BrowserRouter as Router, Route, Link, Switch, Redirect} from "react-router-dom";
import { Button, Modal, Form, Input, InputNumber, Radio, Select,Card, Spin, Icon, Row, Col } from 'antd';
import './CreateRecon.less'

const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const Option = Select.Option;

const urls = require('./../../utility/urls.js').default;
const CreateRecon = Form.create()(
  class extends React.Component {

    constructor(props) { 
        super(props);
        this.state = {
          numVal:'',
          next:false,
          headerName1:"",
          headerName2:""
      };
      this.handleChange = this.handleChange.bind(this);
    }

    handleChange = (e) => {
      this.setState({ reconname: e.target.value });
      console.log("reconname>",this.state.reconname);
    }

    handleChangeNum = (e) => {
     this.setState({ numVal: e.target.value });
      console.log("Number Value>",this.state.numVal);
    }

    handleCreate = (e) => {
      //  console.log("handleCreateETL", this.props.proj)
      e.preventDefault();
        this.props.form.validateFields((err) => {
          if(!err){
            this.saveJob();
            this.setState({ next:true });  
          }
          
        });
     
    };

    
    header1 = (e) => {
      this.setState({ headerName1:e });
      console.log("header1",e);
    }
    header2 = (e) => {
      this.setState({ headerName2:e });
      console.log("header2",e);
    }

    
    saveJob() {
      fetch(urls.urlarray.saveWorkflow, {
        method: "POST",
        //  mode: 'no-cors',
        headers: {
          "Content-Type": "application/json"
          //   'Access-Control-Allow-Origin':'*'
        },
        body: JSON.stringify({
          "projectName":this.props.proj,
          "workflowName":this.state.reconname,
          "workflowType":"Recon",
          // "reconName": this.state.reconname,
        })
      })
        .then(response => {
          console.log("saved successfully", response);
          if (response.status == 200) {
            this.getProductData();
            this.props.initSourceRecon(this.props.proj);
          }
        })
        .catch(err => err);
    }

    
    getProductData() {
      fetch(urls.urlarray.getProjects)
      .then(res => res.json())
      .then(
        (result) => {
          console.log("result111source",result.sourceList)
        },
        (error) => {
          console.log("error in get product source");
          console.log(error);
        }
      )
}

    render() {
      const { visible, onCancel, form, proj } = this.props;
      const {getFieldDecorator} = this.props.form;

      console.log("RECON FORM RENDER",proj)
      
      if(this.state.next) {
        return <Redirect to={{
          pathname: '/taskflow',
          state: { workFlowName: this.state.reconname , workFlowType : "Recon", header1: this.state.headerName1, header2: this.state.headerName2 }
        }}/>
    }

      return (
        <div>
          <Modal 
            visible={visible}
            title="Reconciliation"
            okText="Next"
            onCancel={onCancel}
            onOk={this.handleCreate}
            className="verticalModal"
            >

            <Form>
              <FormItem className="JobName" label="Reconciliation Job Name">
                
                {getFieldDecorator('SourceName',{  rules: [{
                  required: true, message: 'Please enter Source Name',
                  }],})
                  (<Input  placeholder="Enter Reconciliation Job Name" onChange={this.handleChange} />)
                }
              </FormItem> 
              <br/>
              <Row>    
                <Col span={10}>
              <FormItem className="formLabels" label="Reconciliation Relationship" style={{display: 'flex', marginLeft: "0px"}}>
              </FormItem>
              </Col>
                  <Col span={8}>
              <FormItem>
                <Select
                      showSearch
                      placeholder="Reconciliation Relationship"
                      style={{width: "230px"}}
                      defaultValue="One - One"
                    >
                      <Option value="oneToone">One - One </Option>
                      <Option value="oneTomany">One - Many</Option>
                      <Option value="manyToMany">Many - Many</Option>
                    </Select>
                    </FormItem>
                    </Col>
              </Row>
              <Row>    
                <Col span={10}>
              <FormItem className="formLabels" label="Number of Data Sources"/>
              </Col>
              <Col span={8}>
              <FormItem>
                <InputNumber min={1} max={10} defaultValue={1}
                    value={'2'}                      // remove this line when non-static value to be shown               
                    style={{width: "230px"}}/>
                    </FormItem>
                    </Col>               
              
              </Row>

              <Row>
                <Col span={10}>
              <FormItem className="formLabels" label="Source A" ></FormItem>
              </Col>
                  <Col span={8}> 
                  <FormItem>
                    <Select
                    showSearch
                    placeholder="Select Source A"
                    style={{width: "230px"}}
                    defaultValue="Paymentech"
                    onChange={this.header1}
                  >
                    <Option value="BOA-Recon">BOA-Recon</Option>
                    <Option value="First Data">First Data</Option>
                    <Option value="Vantiv">Vantiv</Option>
                    <Option value="Chase Payment">Chase Payment</Option>
                    <Option value="Samson Billing">Samson Billing</Option>
                    <Option value="Paymentech">Paymentech</Option>
                  </Select>            

              </FormItem>
              </Col>
              </Row>    

              <Row>
                <Col span={10}>
                <FormItem  className="formLabels" label="Source B" style={{display: 'flex'}}></FormItem>
                </Col>
                <Col span={8}>
                <FormItem>
                <Select
                    showSearch
                    placeholder="Select Source B"
                    defaultValue="Samson Billing"
                    style={{width: "230px"}}
                    onChange={this.header2}
                    >   
                    <Option value="BOA-Recon">BOA-Recon</Option>
                    <Option value="First Data">First Data</Option>
                    <Option value="Vantiv">Vantiv</Option>
                    <Option value="Chase Payment">Chase Payment</Option>
                    <Option value="Samson Billing">Samson Billing</Option>
                  </Select>
                </FormItem>
                </Col>
              </Row>
            </Form>
          </Modal>
        </div>
      );
    }
  }
);

export default CreateRecon;