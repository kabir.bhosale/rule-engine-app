import React from 'react';
import 'antd/dist/antd.css';
import { Button, Modal, Form, Input, Radio, Select, Icon } from 'antd';
import { Breadcrumb, Steps, Card, Row, Col } from 'antd';
import CompactTable from './../../components/table/index';
import './ReconProcess.less';
//import ToleranceCriteria from '../../components/tolerance-criteria/ToleranceCriteria';
const Step = Steps.Step;
const Option = Select.Option;

const ReconProcess2 = Form.create()(
    class extends React.Component {
        constructor(props) {
            super(props);
            this.state = { visible: '' }

            this.columns = [
                {
                    title: "Paymentech",
                    dataIndex: "FirstData",
                    key: 'FirstData',

                },
               
                {
                    title: "Condition",
                    width: '25%',
                    dataIndex: "Condition",
                    key: 'Condition',

                },
                {
                    title: "Samson Billing",
                    dataIndex: "SecondData",
                    key: 'SecondData'
                },
               
                {
                    title: <div>
                        <center><Icon type="plus-circle" theme="filled" className="plusIcon" /></center>
                    </div>,
                    dataIndex: 'Action',
                    key: 'Action',
                },

            ];

            this.state = {
                dataSource1: [
                    {
                        FirstData: <Input size="small" value="MerchantOrd#" className="inputSmall" />
                        ,
                        Condition: <center>EQUALS</center>,
                        SecondData: <Input size="small" value="Ent_SEQ_NO" className="inputSmall"   />,
                        Action: <div>
                            <center><Icon className="icons" type="delete" /></center>
                        </div>

                    },

                    {
                        FirstData: <Input size="small" value="SubmissionDate" className="inputSmall"  />
                        ,
                        Condition: <center>EQUALS</center>,
                        SecondData: <Input size="small" value="DEPOSIT_DATE" className="inputSmall"  />,
                        Action: <div>
                            <center><Icon className="icons" type="delete" /></center>
                        </div>


                    },

                ],
                dataSource2: [
                    {
                        FirstData:  <Input size="small" placeholder="" />,
                        textinput1:
                            <Input size="small" placeholder="" />

                        ,
                        Condition:
                            <div className="condition">
                                <Input size="small" className="inputNumber " minLength="1" defaultValue="2"></Input>
                                <label >+/- </label>
                                <Select className="newSelect" defaultValue="Absolute">
                                    <Option value="absolute">Absolute</Option>
                                    <Option value="percent">Percentage</Option>
                                </Select>
                            </div>,
                        Billing: <Input size="small" placeholder="" />,
                        textinput2:
                            <Input size="small" placeholder="" />

                        ,
                        Action: <div>
                            <center><Icon className="icons" type="delete" /></center>
                        </div>

                    },

                    {
                        FirstData:  <Input size="small" placeholder="" />,
                        textinput1:
                            <Input size="small" placeholder="sum(fd.amt)" />

                        ,
                        Condition: <div className="condition">
                            <Input size="small" className="inputNumber " minLength="1" defaultValue="2"></Input>
                            <label >+/- </label>
                            <Select className="newSelect" defaultValue="Absolute">
                                <Option value="absolute">Absolute</Option>
                                <Option value="percent">Percentage</Option>
                            </Select>
                        </div>,
                        Billing:  <Input size="small" placeholder="" />,
                        textinput2:
                            <Input size="small" placeholder="sum(sb.amt)" />

                        ,
                        Action: <div>
                            <center><Icon className="icons" type="delete" /></center>
                        </div>

                    },

                ]
            }

        }

        render() {
            const { visible, onCancel, onOk } = this.props;
            console.log("header11111222222222",this.props.columns);

            return (
                <div>
                    <Modal
                        visible={visible}
                        title="Recon Function"
                        onOk={onOk}
                        okText="Next"
                        onCancel={onCancel}
                        className="customModal"
                    >

                        <Breadcrumb className="reconBreadcumb">
                            <Breadcrumb.Item className="processSteps">
                                <Steps size="small" current={1} progressDot>
                                    <Step title="Transaction Selection Criteria" />
                                    <Step title="Matching Rules" />
                                    <Step title="Reconcilation Summary"/>

                                </Steps>
                            </Breadcrumb.Item>
                        </Breadcrumb>
                        <p>Priority :</p>
                        <Card className="cardStyle">
                            <Col span={20}>
                            <label className="subHeading3">Matching Criteria: (Priority 1)</label>
                            </Col>
                            <a>Delete Priority</a>
                            <div className="newCustomTable">
                            <CompactTable columns={this.columns} dataSource={this.state.dataSource1} />
                            </div>
                           <div>
                                {/*      <ToleranceCriteria/>*/}
                           </div>
                            
                        </Card>
                        <br />

                        {/* // <Card className="matchingrulescards">
                        // <Col span={20}>
                        //     <p>Priority 2:</p>   
                        //     </Col>                         
                        //     <a>Delete Priority</a>
                            
                        //  </Card><br></br> */}
                        <div className="tableOptions">
                            <a>Add Priority</a>
                        </div>
                    </Modal>
                </div>
            );
        }
    }
)

export default ReconProcess2;