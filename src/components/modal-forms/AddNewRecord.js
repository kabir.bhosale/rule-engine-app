import React from 'react';
import 'antd/dist/antd.css';
import { Button, Modal, Form, Input, Radio, Select, Progress, Spin, Icon } from 'antd';
import './CreateProject.less'

const Option = Select.Option;
const FormItem = Form.Item;
const { TextArea } = Input;

class Progressing extends React.Component {
  render() {
    return (
      //Spinner 
      <div>
        <center><Spin size="large" tip="Processing ..." /></center>
      </div>
    );
  }
}

// Form Creation
const AddNewRecord = Form.create()(
  class extends React.Component {

    constructor(props){
      super(props)

      this.showSize = false;
      this.fieldTypeOptions = ""

    //  this.onCancel
    }
    


    onCancel(){
      

    }

    render() {

      
      console.log("ADD NEW RECORD RENDER :: ",this.props.fieldType);
      console.log("ADD NEW RECORD RENDER :: ",this.props.sectionType);

      if(this.props.fieldType === "delimited"){
        this.showSize = false;
        this.fieldTypeOptions = "Delimited"
      }else{
        this.showSize = true;
        this.fieldTypeOptions = "Fixed"
      }

      console.log("SHOW SIZE:: ",this.showSize);

      const { visible, onCancel, onCreate, form, visible1, onCancel1, onCreate1, } = this.props;
      const { getFieldDecorator } = form;
      const formItemLayout = {
        labelCol: { span: 9 },
        wrapperCol: { span: 14 },
      };

      return (

        <div>
          <Modal 
            visible={visible}
            title="Add New Record"
            okText="Add"
            onCancel={onCancel}
            onOk={onCreate}
            className="verticalModal"
            >
            <Form layout="vertical">{/* Project Title */}
              
            <FormItem label="Enter Field Index">
                {getFieldDecorator('fieldIndex', {
                  rules: [{ required: true, message: 'Please enter field Index!' }],
                })
                  (<Input />)}
              </FormItem>
              
              <FormItem label="Enter Field Name">
                {getFieldDecorator('fieldName', {
                  rules: [{ required: true, message: 'Please enter field Name!' }],
                })
                  (<Input />)}
              </FormItem>


              <FormItem  label="Select Data Type" >
                {getFieldDecorator('fieldType', {
                  rules: [{ required: true, message: 'Please select Data Type!' }],
                })
                (
                  // <div className="dropdown"> 
                  <Select placeholder="Select Data Type">
                    <Option value="Integer">Integer</Option>
                    <Option value="String">String</Option>
                    <Option value="Number">Number</Option>
                    <Option value="Date">Date</Option>
                  </Select>
                  // </div>
                )}
              </FormItem>

              {/* <FormItem label="Select Field Type">

                 {getFieldDecorator('type')
                (
                  
                  <Select placeholder="Select Field Type" defaultValue = {this.fieldTypeOptions} >
                     <Option value={this.fieldTypeOptions}>{this.fieldTypeOptions}</Option>
                  </Select>
                  
                )} 
              </FormItem> */}

         { (this.showSize?( <FormItem label="Enter Size">
                {getFieldDecorator('fieldLength') 
                  (<Input />)
                  }

                  {/* <Input /> */}
              </FormItem>  ) : (<div></div>))

         }

              <FormItem label="Enter Field Format">
                {getFieldDecorator('fieldFormat')
                  (<Input />)}
              </FormItem>

               


              {/* <FormItem label="Select Encoding Type">
                {getFieldDecorator('encodingType', {
                  rules: [{ required: true, message: 'Please Select Encoding Type!' }],
                })
                (
                  // <div className="dropdown">
                  <Select placeholder="Select Encoding Type">
                    <Option value="ASCII">ASCII</Option>
                    <Option value="ISO 646">ISO 646</Option>
                    <Option value="CP37">CP37</Option>
                    <Option value="KOI8-R">KOI8-R</Option>
                  </Select>
                  // </div>
                )}
              </FormItem> */}

              {/* Document Type */}
            {/*   
              <div>
              {/* File header */}
              {/* <FormItem className="collection-create-form_last-form-item" label="Does the file have headers ?" style={{display: 'flex', flexDirection: 'row', marginTop:"-25px", marginBottom:"0px"}}>
                {getFieldDecorator('headerSet', {
                  rules: [{ required: true, message: 'Please specify whether the file has Headers!' }],
                })(
                  <div className="radiodiv">
                  <Radio.Group>
                    <Radio value="yes" style={{marginLeft:"40px"}}>Yes</Radio>
                    <Radio value="no" style={{marginLeft:"10px"}}>No</Radio>
                  </Radio.Group>
                  </div>
                )}
              </FormItem> 
              </div> */}
            </Form>
          </Modal>
          {/* Modal for Spinner */}
          <Modal
            visible={visible1}
            closable={false}
            centered={true}
            okButtonProps={{ disabled: true }}
            cancelButtonProps={{ disabled: true }}
            bodyStyle={{ height: "100px" }}
          >
            <Progressing />
          </Modal>
        </div>
      );
    }
  }
);
export default AddNewRecord;
