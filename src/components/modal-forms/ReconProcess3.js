import React, { Component } from 'react';
import {Modal, Row, Col, Select, Form, Card, Breadcrumb, Steps} from 'antd';

const Step = Steps.Step;
const ReconProcess3 = Form.create()(
    class extends React.Component {
    constructor(props) 
    {
        super(props);
        this.state = { visible: '' };
    }
    render()
    {         
        const { visible, onCancel, onOk } = this.props;
        return(        
            <Modal
                visible={visible}
                title="Recon Function"
                onOk={onOk}
                okText="Next"
                onCancel={onCancel}
                className="customModal">
                
                <Breadcrumb className="reconBreadcumb">
                            <Breadcrumb.Item className="processSteps">
                                <Steps size="small" current={2} progressDot>
                                    <Step title="Transaction Selection Criteria" />
                                    <Step title="Matching Rules" />
                                    <Step title="Reconcilation Summary"/>

                                </Steps>
                            </Breadcrumb.Item>
                        </Breadcrumb>

                <p className="formLabels">Reconcilation Summary :</p>
                
                <Card className="cardStyle">
                    
                    <div className="divinsidecards">
                        <Row align="bottom">
                            <Col span={12}>
                                <label className="subHeading3">Paymentech</label>
                               
                            </Col>
                            <Col span={12}>
                                <label className="subHeading3">Samson Billing</label>
                                
                            </Col>
                        </Row>
                        <br/>
                        <Row align="bottom">
                            <Col span={12}>
                            <Select className="selectElement" defaultValue="Amount">
                                <Option value="Amount">Amount</Option>
                            </Select>
                               
                            </Col>
                            <Col span={12}>
                                <Select className="selectElement" defaultValue="ORIGINAL_AMT">
                                    <Option value="ORIGINAL_AMT">ORIGINAL_AMT</Option>
                                    <Option value="ACTV_AMT">ACTV_AMT</Option>
                                    <Option value="INTERNAL_ACTV_AMT">INTERNAL_ACTV_AMT</Option>
                                </Select>
                            </Col>
                        </Row>

                    </div>

                </Card>
            </Modal>

        
    );
    }
       
}
)
export default ReconProcess3;