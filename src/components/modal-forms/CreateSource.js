import React from "react";
import "antd/dist/antd.css";
import { Button, Modal, Form, Input, InputNumber, Radio, Select, Spin, Icon, Popover} from "antd";
import { BrowserRouter as Router, Route, Link, Switch, Redirect} from "react-router-dom";
import "./CreateSource.less";

const urls=require('./../../utility/urls').default;
const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const Option = Select.Option;

const CreateETLForm = Form.create()(
  class extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        nextETL: false,
        srcname: '',
        inputsrc: ""
      }; 
    }

    handleCreateETL = (e) => {
      this.props.form.validateFields((err, values) => {
            if (err) {
                return;
            }
             
            this.saveSource();
            this.setState({ nextETL: true });
            console.log("ELE Changed to TRUE"); 
          

        });
        console.log("handleCreateETL", this.props.proj)

      /* e.preventDefault();
        this.props.form.validateFields(() => {
          if(this.state.srcname!='')
          {
            this.saveSource();
            console.log("ELE Changed to TRUE"); 
          }  
        }       
        );   */  
    }

    handleChange = (e) => {
     
     
        this.setState({ srcname: e.target.value });
        console.log("srcname",this.state.srcname);
      
     
    }

    saveSource() {
      fetch(urls.urlarray.saveWorkflow, {
        method: "POST",
        //  mode: 'no-cors',
        headers: {
          "Content-Type": "application/json"
          //   'Access-Control-Allow-Origin':'*'
        },
        body: JSON.stringify({
          "projectName":this.props.proj,
          "workflowName":this.state.srcname,
          "workflowType":"ETL",
          // name: this.state.srcname,
        })
      })
        .then(response => {
          console.log("saved successfully", response);
          if (response.status == 200) {
            this.getProductData();
            this.setState({ nextETL: true });
            this.props.initSourceRecon(this.props.proj);
          }
        })
        .catch(err => err);
    }

    
    getProductData() {
      fetch(urls.urlarray.getProjects)
      .then(res => res.json())
      .then(
        (result) => {
          console.log("result111source",result.sourceList)
        },
        (error) => {
          console.log("error in get product source");
          console.log(error);
        }
      )
}

    render() {
      const { visible, onCancel,  proj } = this.props;
      const { getFieldDecorator } = this.props.form;

      console.log("ETL FORM RENDER",proj)
      if (this.state.nextETL) {
        return <Redirect to={{
          pathname: '/taskflow',
          state: { workFlowName: this.state.srcname , workFlowType : "ETL"  }
        }}/>
      }

      return (
        <div>
          <Modal
            visible={visible}
            title="Source"
            okText="Next"
            onCancel={onCancel}
            onOk={this.handleCreateETL}
            className="formModal"
          >
            <Form>
              <FormItem  label="Source Name" className="subHeading3">

                {getFieldDecorator('title',{  rules: [{
                  required: true, message: 'Please enter Source Name',
                  }],})
                  (<Input  value="inputsrc" placeholder="Enter Source Name" onChange={this.handleChange}/>)
                }

              </FormItem>

              <br />
              <br/>

            </Form>
          </Modal>
        </div>
      );
    }
  }
);
export default CreateETLForm;
