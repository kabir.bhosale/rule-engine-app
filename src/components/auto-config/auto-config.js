import React, { Component } from 'react';
import { Popconfirm, Icon, Card, Button} from 'antd';
import CompactTable from '../../components/table/index';
import './auto-config.less';

  
class AutoConfig extends Component {
    constructor(props) {
      super(props);
      this.columns = [
        {
          title: "Customer Name",
          dataIndex: "CustomerName",
          key: 'CustomerName', 
        },
        {
          title: "Customer A/C",
          dataIndex: "CustomerAc",
          key: 'CustomerAc',
          
        },
        {
          title: "Transaction ID",
          dataIndex: "transactionID", 
          key: 'transactionID',
          
        },
        {
            title: "Transaction Amt",
            dataIndex: "transactionAmt", 
            key: 'transactionAmt'
        },
        { 
          title: 'Transaction Ref#', 
          dataIndex: 'transactionRef', 
          key: ' transactionRef',           
        }
      ];
  
      this.state = {
        dataSource: [
          {
            CustomerName: 'John',    
            CustomerAc: 987654321,        
            transactionID: 123654,    
            transactionAmt: 32158,
            transactionRef:123654788965
          },
          {
            CustomerName: 'Chris',    
            CustomerAc: 987654321,        
            transactionID: 123654,    
            transactionAmt: 32158,
            transactionRef:123654788965
          },
          {
            CustomerName: 'Andy',    
            CustomerAc: 987654321,        
            transactionID: 123654,    
            transactionAmt: 32158,
            transactionRef:123654788965
          },
          {
            CustomerName: 'David',    
            CustomerAc: 987654321,        
            transactionID: 123654,    
            transactionAmt: 32158,
            transactionRef:123654788965
          },
          {
            CustomerName: 'DSK',    
            CustomerAc: 987654321,        
            transactionID: 123654,    
            transactionAmt: 32158,
            transactionRef:123654788965
          }

        ],
        
      };
    }
  
    render() {
      return (
        <Card className="cardStyle">
        
         {/*  <h1>Auto Configuration</h1> */}
         
          <CompactTable className = "ant-table-thead" columns={this.columns}  dataSource={this.state.dataSource} pagination={{ pageSize: 10 }} size="small" bordered/>
         
                  
        </Card>
      );
    }
  }
  
  export default AutoConfig;