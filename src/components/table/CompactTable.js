import React, { Component } from 'react';

import { Table, Popconfirm, Icon, Card, Button} from 'antd';
// import 'antd/dist/antd.css';

import './CompactTable.less';
  
const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
  },
  getCheckboxProps: record => ({
    disabled: record.name === 'Disabled User', // Column configuration not to be checked
    name: record.name,
  }),
};

class CompactTable extends Component {
    constructor(props) {
      super(props);
        this.state = {
        dataSource: this.props.dataSource,
        columns:this.props.columns,
        
      };      

      
    
    }

    
    render() {
    //console.log('In compatible' , this.props.dataSource)
      return (
            <div>
              
                <Table className="ctable" columns={this.props.columns} dataSource={this.props.dataSource} pagination={{ pageSize: 10 }} size="small" bordered="true" />
                
            </div>
      );
      
    }
  }
  
  export default CompactTable;
