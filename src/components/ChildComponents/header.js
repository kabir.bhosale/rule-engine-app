﻿import React, { Component } from "react";
import CreateReactClass from "create-react-class";
import { connect } from "react-redux";

import ReactDOM from "react-dom";
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";

import { withStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Modal from "@material-ui/core/Modal";
import Button from "@material-ui/core/Button";
//import Checkbox from '@material-ui/core/Checkbox';
import { Checkbox } from "antd";
import Icon from "@material-ui/core/Icon";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";
import AddIcon from "@material-ui/icons/AddCircle";
import { stat } from "fs";

var Modal_style = {
  position: "absolute",
  width: "300px",
  backgroundColor: "#fff",
  boxShadow: "5px",
  padding: "10px 40px",
  margin: "30px 20px 10px 550px",
  borderRadius: "2px"
};
var Modal_button_style = {
  margin: "15px",
  fontSize: "12px"
};
var Color_button_style = {
  margin: "10px",
  fontSize: "12px",
  background: "#1890ff",
  color: "white"
};
var Modal_icon_style = {
  margin: "-5px"
};
var Add_icon_style = {
  margin: "-10px",
  focusVisible: "false",
  color: "#1890ff"
};

var ExpansionPanel_style = {
  fontSize: "14px"
  // fontWeight: fontWeightRegular
};

export class HeaderBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      currentHeader: {},
      changeheader: 1
    };
  }

  handleDelete = currentHeader => {
    if (currentHeader == undefined) {
      //  currentHeader = {};
      //this.props.requestResult.headers.push(currentHeader);
    }
    // alert(JSON.stringify(currentHeader));
    //  alert(this.props.requestResult.headers.indexOf(currentHeader));
    this.props.requestResult.headers.splice(
      this.props.requestResult.headers.indexOf(currentHeader),
      1
    );
    //this.setState({ open: true, headerid: currentHeader.id, headerkey: currentHeader.key, headervalue: currentHeader.value, headermethod: currentHeader.method, headerrequired: currentHeader.required });
    this.setState({ currentHeader: currentHeader });
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
    this.state.currentHeader[e.target.name] = e.target.value;
  };

  onCheckedChange = e => {
    this.setState({ [e.target.name]: e.target.checked });
    this.state.currentHeader[e.target.name] = e.target.checked;
  };

  handleOpen = currentHeader => {
    //alert(this.props.requestResult.headers);
    if (currentHeader == undefined) {
      currentHeader = {};
      this.props.requestResult.headers.push(currentHeader);
    }

    this.setState({
      open: true,
      headerid: currentHeader.id,
      headerkey: currentHeader.key,
      headervalue: currentHeader.value,
      headermethod: currentHeader.method,
      headerrequired: currentHeader.required
    });
    this.setState({ open: true, currentHeader: currentHeader });
  };

  handleSave = () => {
    this.state.currentHeader.id = this.state.headerid;
    this.state.currentHeader.key = this.state.headerkey;
    this.state.currentHeader.value = this.state.headervalue;
    this.state.currentHeader.method = this.state.headermethod;

    this.state.currentHeader.required = this.state.headerrequired;

    this.setState({ open: false });
  };
  handleClose = () => {
    this.props.requestResult.headers.pop();
    this.setState({ open: false });
  };

  render() {
    return (
      <div>
        {renderHeaders(this.props, this)}
        <div className="col-md-3 col-sm-3 col-xs-12">
          <Modal
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={this.state.open}
            onClose={() => {
              this.state.setState({ open: false });
            }}
          >
            <div style={Modal_style}>
              <Typography
                variant="h6"
                id="modal-title"
                style={{ fontSize: "20px" }}
              >
                Add Header{" "}
                <hr style={{ height: "1px", backgroundColor: "black" }} />
              </Typography>
              <Typography
                variant="subtitle1"
                id="simple-modal-description"
                style={ExpansionPanel_style}
              >
                <label className="control-label">Key</label>
                <br />
                <input
                  type="hidden"
                  name="headerid"
                  value={this.state.headerid}
                  onChange={this.onChange}
                  className="form-control"
                />
                <input
                  type="text"
                  name="headerkey"
                  value={this.state.headerkey}
                  onChange={this.onChange}
                  className="form-control"
                />

                <label className="control-label">Value</label>
                <br />
                <input
                  type="text"
                  name="headervalue"
                  value={this.state.headervalue}
                  onChange={this.onChange}
                  className="form-control"
                />

                <label className="control-label">Mandatory</label>
                <Checkbox
                  name="headerrequired"
                  checked={this.state.headerrequired}
                  onChange={this.onCheckedChange}
                  value="checkedMandatory"
                  style={{ color: "#1890ff" }}
                />
                <hr style={{ height: "1px", backgroundColor: "black" }} />
              </Typography>

              <Typography id="modal-footer" style={ExpansionPanel_style}>
                <Button
                  variant="contained"
                  onClick={this.handleClose}
                  style={Modal_button_style}
                >
                  Cancel
                </Button>
                <Button
                  variant="contained"
                  onClick={this.handleSave}
                  style={Color_button_style}
                >
                  Save
                </Button>
              </Typography>
            </div>
          </Modal>
        </div>
      </div>
    );
  }
}

//export const HeaderBar = props => (
//    <div className="col-md-12 col-sm-12 col-xs-3" >
//        {renderHeaders(props)}
//        {renderPopUp(props)}
//    </div>
//);

function renderHeaders(props, state) {
  return (
    <table
      className="table table-bordered center"
      style={{ paddingTop: 10, textAlign: "center" }}
    >
      <thead>
        <tr>
          <th>Id</th>
          <th>Key</th>
          <th>Value</th>
          <th>Required</th>
          <th>
            {" "}
            <IconButton
              disableRipple="true"
              variant="fab"
              color="primary"
              aria-label=" properp"
              style={Add_icon_style}
              onClick={() => {
                state.handleOpen();
              }}
            >
              <AddIcon />
            </IconButton>
          </th>
        </tr>
      </thead>
      {renderBody(props, state)}
    </table>
  );
}

function renderBody(props, state) {
  if (
    !(
      props.requestResult.headers == undefined ||
      props.requestResult.headers.length < 1
    )
  ) {
    return (
      <tbody>
        {props.requestResult.headers.map(header => (
          <tr key={header.id}>
            <td>
              <a href={"/request/" + header.id}> {header.id}</a>
            </td>
            <td>{header.key}</td>
            <td>{header.value}</td>
            <td>{header.required ? "Required" : ""}</td>
            <td>
              <IconButton
                onClick={() => {
                  state.handleOpen(header);
                }}
                style={Modal_icon_style}
                aria-label="Edit"
              >
                <EditIcon />
              </IconButton>
              <IconButton
                onClick={() => {
                  state.handleDelete(header);
                }}
                style={Modal_icon_style}
                aria-label="Delete"
              >
                <DeleteIcon />
              </IconButton>
            </td>
          </tr>
        ))}
      </tbody>
    );
  } else {
    return (
      <tbody>
        <tr>
          <td colSpan="5">Please add header</td>
        </tr>
      </tbody>
    );
  }
}

export default connect()(HeaderBar);
