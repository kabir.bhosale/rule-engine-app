﻿import React, { Component } from "react";
import { connect } from "react-redux";
import { Card } from "antd";
//import { TreeSelect } from 'antd';
import Chips, { Chip } from "react-chips";

//const SHOW_PARENT = TreeSelect.SHOW_PARENT;

//const treeData = [{
//    title: 'Group1',
//    value: '0-0',
//    key: '0-0',
//    children: [{

//    }],
//}, {
//    title: 'Group2',
//    value: '0-1',
//    key: '0-1',
//    children: [{

//    }],
//}];

export class AppInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      method: this.props.requestResult.method,
      // value: ['0-0-0'],
      group: []
    };
  }

  componentWillReceiveProps(nextProps) {
    // You don't have to do this check first, but it can help prevent an unneeded render
    // alert(JSON.stringify(nextProps.requestResult.group));
    if (nextProps.requestResult.method != this.state.method) {
      this.setState({
        name: nextProps.requestResult.name,
        method: nextProps.requestResult.method,
        url: nextProps.requestResult.url,
        summary: nextProps.requestResult.summary,
        type: nextProps.requestResult.type,
        port: nextProps.requestResult.port,
        group: nextProps.requestResult.group
      });
      //if (nextProps.requestResult.group == null) {
      //    this.setState({ group: [] })
      //}
      //else
      //    this.setState({
      //        group: nextProps.requestResult.group
      //    })
    }
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
    this.props.requestResult[e.target.name] = e.target.value;
  };

  onGroupChange = chips => {
    // alert("chips"+chips);
    this.setState({ group: chips });
    // alert("doka" + this.state.group);
    this.props.requestResult.group = chips;
    // alert(this.state.group);
    //alert(typeof (this.state.group));
  };

  render() {
    return (
      <div style={{ paddingTop: 10 }}>
        <Card title="General">
          <form
            id="demo-form2"
            data-parsley-validate
            className="form-horizontal form-label-left"
          >
            {/* <div className="form-group">
              <label
                className="control-label col-md-12 col-sm-12 col-xs-12 form-label-left"
                htmlFor="name"
                style={{ textAlign: "left" }}
              >
                <span className="required">*</span> Name:
              </label>
              
            </div> */}
            <div className="col-md-12 form-group">
              <div className="col-md-2 keyfield">
                <span className="required">*</span> Name &nbsp;&nbsp;:
              </div>
              <div className="col-md-10">
                <input
                  type="text"
                  id="name"
                  name="name"
                  required
                  value={this.state.name}
                  className="form-control"
                  onChange={this.onChange}
                />
              </div>
            </div>
            {/* <div className="form-group">
              <label
                className="control-label col-md-3 col-sm-12 col-xs-3"
                htmlFor="method"
                style={{ textAlign: "left" }}
              >
                <span className="required">*</span> Method:
              </label>
              <label
                className="control-label col-md-3 col-sm-12 col-xs-3"
                htmlFor="path"
                style={{ textAlign: "left" }}
              >
                <span className="required">*</span> Path:
              </label>
            </div> */}
            <div className="col-md-12 form-group">
              <div className="col-md-2 keyfield">
                <span className="required">*</span> Method &nbsp;&nbsp;:
              </div>
              <div className="col-md-3">
                <select
                  name="method"
                  id="method"
                  value={this.state.method}
                  className="form-control col-md-7 col-xs-12"
                  onChange={this.onChange}
                >
                  <option value="1">POST</option>
                  <option value="2">PUT</option>
                  <option value="3">GET</option>
                  <option value="4">PATCH</option>
                </select>
              </div>
              <div className="col-md-1 keyfield">
                <span className="required">*</span> Path :
              </div>
              <div className="col-md-6">
                <input
                  type="text"
                  id="path"
                  name="url"
                  required="required"
                  value={this.state.url}
                  onChange={this.onChange}
                  className="form-control col-md-7 col-xs-12"
                />
              </div>
            </div>

            <div className="col-md-12 form-group">
              <div className="col-md-2 keyfield">
                <span className="required">*</span> Type &nbsp;&nbsp;:
              </div>
              <div className="col-md-3">
                <select
                  name="type"
                  id="type"
                  value={this.state.type}
                  className="form-control col-md-7 col-xs-12"
                  onChange={this.onChange}
                >
                  <option value="1">XML</option>
                  <option value="2">JSON</option>
                </select>
              </div>
              <div className="col-md-1 keyfield">
                <span className="required">*</span> Port :
              </div>
              <div className="col-md-6">
                <input
                  type="text"
                  id="port"
                  name="port"
                  required="required"
                  value={this.state.port}
                  onChange={this.onChange}
                  className="form-control col-md-7 col-xs-12"
                />
              </div>
            </div>
            <div className="col-md-12 form-group">
              <div className="col-md-2 keyfield">
                <span className="required">*</span> Description &nbsp;&nbsp;:
              </div>
              <div className="col-md-10">
                <textarea
                  className="form-control"
                  rows={3}
                  onChange={this.onChange}
                  placeholder="summary"
                  name="summary"
                  id="summary"
                  value={this.state.summary}
                  required
                />
              </div>
            </div>
          </form>
        </Card>
      </div>
      //   <div className="x_panel">
      //     <div className="clearfix" />
      //     <form
      //       id="demo-form2"
      //       data-parsley-validate
      //       className="form-horizontal form-label-left"
      //     >
      //       <div className="form-group">
      //         <label
      //           className="control-label col-md-12 col-sm-12 col-xs-12 form-label-left"
      //           htmlFor="name"
      //           style={{ textAlign: "left" }}
      //         >
      //           <span className="required">*</span> Name:
      //         </label>
      //         <div className="col-md-12 col-sm-12 col-xs-3">
      //           <input
      //             type="text"
      //             id="name"
      //             name="name"
      //             required
      //             value={this.state.name}
      //             className="form-control"
      //             onChange={this.onChange}
      //           />
      //         </div>
      //       </div>
      //       <div className="form-group">
      //         <label
      //           className="control-label col-md-3 col-sm-12 col-xs-3"
      //           htmlFor="method"
      //           style={{ textAlign: "left" }}
      //         >
      //           <span className="required">*</span> Method:
      //         </label>
      //         <label
      //           className="control-label col-md-3 col-sm-12 col-xs-3"
      //           htmlFor="path"
      //           style={{ textAlign: "left" }}
      //         >
      //           <span className="required">*</span> Path:
      //         </label>
      //       </div>

      //       <div className="form-group">
      //         <div className="col-md-3 col-sm-12 col-xs-3">
      //           <select
      //             name="method"
      //             id="method"
      //             value={this.state.method}
      //             className="form-control col-md-7 col-xs-12"
      //             onChange={this.onChange}
      //           >
      //             <option value="1">POST</option>
      //             <option value="2">PUT</option>
      //             <option value="3">GET</option>
      //             <option value="4">PATCH</option>
      //           </select>
      //         </div>
      //         <div className="col-md-9 col-sm-12 col-xs-3">
      //           <input
      //             type="text"
      //             id="path"
      //             name="url"
      //             required="required"
      //             value={this.state.url}
      //             onChange={this.onChange}
      //             className="form-control col-md-7 col-xs-12"
      //           />
      //         </div>
      //       </div>
      //       <div className="form-group">
      //         <label
      //           className="control-label col-md-3 col-sm-12 col-xs-3"
      //           htmlFor="type"
      //           style={{ textAlign: "left" }}
      //         >
      //           <span className="required">*</span> Type:
      //         </label>
      //         <label
      //           className="control-label col-md-3 col-sm-12 col-xs-3"
      //           htmlFor="port"
      //           style={{ textAlign: "left" }}
      //         >
      //           <span className="required">*</span> Port:
      //         </label>
      //       </div>
      //       <div className="form-group">
      //         <div className="col-md-3 col-sm-12 col-xs-3">
      //           <select
      //             name="type"
      //             id="type"
      //             value={this.state.type}
      //             className="form-control col-md-7 col-xs-12"
      //             onChange={this.onChange}
      //           >
      //             <option value="1">XML</option>
      //             <option value="2">JSON</option>
      //           </select>
      //         </div>
      //         <div className="col-md-9 col-sm-12 col-xs-3">
      //           <input
      //             type="text"
      //             id="port"
      //             name="port"
      //             required="required"
      //             value={this.state.port}
      //             onChange={this.onChange}
      //             className="form-control col-md-7 col-xs-12"
      //           />
      //         </div>
      //       </div>

      //       <div className="form-group">
      //         <label
      //           className="control-label col-md-12 col-sm-12 col-xs-12 form-label-left"
      //           htmlFor="last-name"
      //           style={{ textAlign: "left" }}
      //         >
      //           <span className="required">*</span> Description:
      //         </label>
      //         <div className="col-md-12 col-sm-6 col-xs-12">
      //           <textarea
      //             className="form-control"
      //             rows={3}
      //             onChange={this.onChange}
      //             placeholder="summary"
      //             name="summary"
      //             id="summary"
      //             value={this.state.summary}
      //             required
      //           />
      //         </div>
      //       </div>
      //     </form>
      //   </div>
    );
  }
}

export default connect()(AppInfo);
