﻿import React, { Component } from 'react';
import CreateReactClass from "create-react-class";
import { connect } from 'react-redux';

import ReactDOM from "react-dom";
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";

import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';



var Modal_style = {
    position: 'absolute',
    width: '500px',
    backgroundColor: '#fff',
    boxShadow: '5px',
    padding: '10px 40px',
    margin: '200px 20px 10px 450px'
};
var Modal_button_style = {
    margin: '15px',
    fontSize: '12px'
};
var Modal_icon_style = {
    margin: '-5px'
};
var Add_icon_style = {
    margin: '-10px',
    focusVisible: "false"

};


var ExpansionPanel_style = {
    fontSize: '18px'
    // fontWeight: fontWeightRegular
};

export class Validator extends Component {
    constructor(props) {
        super(props);
        if (props.state.currentValidator === undefined) {
            props.state.currentValidator = {};
        }
        this.state = {
            open: false,
            currentvalidation: props.state.currentValidator,
            rule: props.state.currentValidator.rule,
            value: props.state.currentValidator.value,
            showModal: true
        };

    }

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
        this.state.currentValidator[e.target.name] = e.target.value;
    }

    handleOpen = (currentHeader) => {
        if (currentHeader == undefined) {
            currentHeader = {};
            this.props.requestResult.headers.push(currentHeader);
        };

        this.setState({ open: true, headerid: currentHeader.id, headerkey: currentHeader.key, headervalue: currentHeader.value, headermethod: currentHeader.method, headerrequired: currentHeader.required });
        this.setState({ open: true, currentHeader: currentHeader });
    };

    handleSave = () => {
        this.state.currentValidator.rule = this.state.rule;
        this.state.currentValidator.value = this.state.value;
        //this.state.currentHeader.value = this.state.headervalue;
        //this.state.currentHeader.method = this.state.headermethod;
        //this.state.currentHeader.required = this.state.headerrequired;

        this.props.state.open = false;
        this.setState({ open: false });
    }
    handleClose = () => {
        this.props.handleClose();
    };
    
    render() {
        return (

            <div style={Modal_style}>
                <Typography variant="h6" id="modal-title" style={{ fontSize: '20px' }}>
                    Add Validations <hr />
                </Typography>
                <Typography variant="subtitle1" id="simple-modal-description" style={ExpansionPanel_style}>
                    <label>Rule</label>
                    <br />
                    <input type="hidden" name="rule" value={this.state.rule} onChange={this.onChange}/>
                    <select>
                        <option>Equals to</option>    
                        <option>Less Than</option>    
                        <option>Less Than Equals to</option>    
                        <option>Greater Than</option>
                        <option>Greater Than Equals to</option>    
                    </select>
                    <br />
                    <label>Value</label>
                    <br />
                    <input type="text" name="value" value={this.state.value} onChange={this.onChange} />
                    <hr />
                </Typography>

                <Typography id="modal-footer" style={ExpansionPanel_style} >
                    <Button variant="outlined" onClick={this.handleClose} style={Modal_button_style} >Cancel</Button>
                    <Button variant="outlined" onClick={this.handleSave} color="secondary" style={Modal_button_style}>Save</Button>
                </Typography>
            </div>


        );
    }
}
export default connect()(Validator);