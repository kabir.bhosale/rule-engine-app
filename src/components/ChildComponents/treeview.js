﻿import "rc-tree/assets/index.css";
import React, { Component } from "react";
import { connect } from "react-redux";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import Tree, { TreeNode } from "rc-tree";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import Modal from "@material-ui/core/Modal";
import Typography from "@material-ui/core/Typography";
import AddIcon from "@material-ui/icons/AddCircle";
import Tooltip from "rc-tooltip";
// import Button from "@material-ui/core/Button";
//import  Checkbox  from '@material-ui/core/Checkbox';
import { Checkbox, Button } from "antd";
import "./react-treeview.css";
import "./contextmenu.less";
import CompactTable from "./../../components/table/index";
import { Icon } from "antd";
import NativeListener from "react-native-listener";
import { EditableTable } from "./testdata";
import Chips, { Chip } from "react-chips";

var Modal_style = {
  position: "absolute",
  width: "300px",
  backgroundColor: "#fff",
  boxShadow: "5px",
  padding: "10px 40px",
  margin: "30px 20px 10px 550px",
  borderRadius: "2px"
};
var Modal_button_style = {
  margin: "15px",
  fontSize: "12px"
};
var Color_button_style = {
  margin: "10px",
  fontSize: "12px",
  background: "#1890ff",
  color: "white"
};
var Modal_icon_style = {
  margin: "-5px"
};
var Add_icon_style = {
  margin: "-10px",
  focusVisible: "false",
  //background: '#03a9f4',
  color: "#1890ff"
};
var dataJson = [];

export class PropertyView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      currentproperty: { id: this.createID() },
      //targets: [{ name: 'Root' }],
      currentstateproperty: {},
      properties: [],
      showModal: false,
      id: 0,
      expandedKeys: ["0-0-0-key"],
      autoExpandParent: true,
      checkedKeys: ["0-0-0-key"],
      selectedKeys: [],
      treeData: [],
      currentvalidation: {},
      rootproperty: {},
      group: [],
      groupHide: true
    };
    this.mangeTreeviewbusiness(props, this);
  }

  s4 = () => {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  };

  createID = () => {
    var guid =
      this.s4() +
      this.s4() +
      "-" +
      this.s4() +
      "-" +
      this.s4() +
      "-" +
      this.s4() +
      "-" +
      this.s4() +
      this.s4() +
      this.s4();
    return guid;
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
    this.state.currentproperty[e.target.name] = e.target.value;
  };

  onCheckedChange = e => {
    this.setState({ [e.target.name]: e.target.checked });
    this.state.currentproperty[e.target.name] = e.target.checked;
  };

  onVChange = e => {
    this.setState({ [e.target.name]: e.target.value });
    this.state.currentvalidation[e.target.name] = e.target.value;
    if ([e.target.name] == "rule" && e.target.value == 6) {
      this.setState({ groupHide: false });
    } else {
      this.setState({ groupHide: true });
    }
  };

  onRightMenuClick = info => {
    console.log(info);
    console.log("hi", info.node.props);
    this.setState({ selectedKeys: [info.node.props.eventKey] });

    this.renderCm(info);
    return false;
  };
  getContainer() {
    if (!this.cmContainer) {
      this.cmContainer = document.createElement("div");
      document.body.appendChild(this.cmContainer);
    }
    return this.cmContainer;
  }

  onGroupChange = chips => {
    // alert("chips"+chips);
    this.setState({ group: chips });
    // alert("doka" + this.state.group);
    this.props.requestResult.group = chips;
    // alert(this.state.group);
    //alert(typeof (this.state.group));
  };

  renderCm(info) {
    if (this.toolTip) {
      ReactDOM.unmountComponentAtNode(this.cmContainer);
      this.toolTip = null;
    }
    this.toolTip = (
      <Tooltip
        trigger="click"
        placement="bottomRight"
        prefixCls="rc-tree-contextmenu"
        defaultVisible
        overlay={
          <div>
            <a onClick={this.handleAddChild(info)}>Add Child</a>
          </div>
        }
      >
        <span />
      </Tooltip>
    );
    const container = this.getContainer();
    Object.assign(this.cmContainer.style, {
      position: "absolute",
      left: `${info.event.pageX}px`,
      top: `${info.event.pageY}px`
    });

    ReactDOM.render(this.toolTip, container);
  }

  handleDeletion = validator => {
    // alert(JSON.stringify(validator));
    if (validator == undefined) {
    }
    this.state.currentproperty.validations.splice(
      this.state.currentproperty.validations.indexOf(validator),
      1
    );
    this.setState({
      currentvalidation: validator,
      rule: validator.rule,
      value: validator.value
    });
  };

  handleValidate = validator => {
    //alert(JSON.stringify(validator));
    if (validator == undefined) {
      validator = {};
      if (
        this.state.currentproperty.validations == undefined ||
        this.state.currentproperty.validations == null
      ) {
        this.state.currentproperty.validations = [];
      }
      this.state.currentproperty.validations.push(validator);
    }

    var tmp = ResolveNumber(validator.rule);
    this.setState({
      currentvalidation: validator,
      rule: tmp,
      value: validator.value
    });
    this.setState({ showModal: true });
    this.setState({ open: true });
  };

  handleValidationSave = () => {
    //alert(this.state.rule);
    this.state.currentvalidation.rule = ResolveString(this.state.rule);
    this.state.currentvalidation.value = this.state.value;

    this.setState({ open: false });
  };

  mangeTreeviewbusiness = (nextProps, _this) => {
    if (nextProps.requestResult.id != this.state.id) {
      //  alert("hi" + JSON.stringify(nextProps.requestResult.properties));
      if (nextProps.requestResult.id > 0) {
        _this.setState({ properties: nextProps.requestResult.properties });
        if (
          nextProps.requestResult.properties == undefined ||
          nextProps.requestResult.properties.length <= 0
        ) {
          nextProps.requestResult.properties = [];
          _this.state.currentproperty = { id: this.createID() };
          _this.state.currentproperty.name = "Add Property";
          _this.state.currentproperty.validations = [];
          nextProps.requestResult.properties.push(this.state.currentproperty);
          _this.setState({ name: this.state.currentproperty.name });
        } else {
          _this.changestate(
            "id",
            _this.state.currentproperty.id,
            _this.createID()
          );
          _this.changestate("name", _this.state.currentproperty.name, "");
          _this.changestate("alias", _this.state.currentproperty.alias, "");
          _this.changestate("type", _this.state.currentproperty.type, "");
          _this.changestate(
            "description",
            _this.state.currentproperty.description,
            ""
          );
          _this.changestate(
            "required",
            _this.state.currentproperty.required,
            false
          );

          //_this.setState(
          //    {
          //        currentproperty: nextProps.requestResult.properties[0]
          //    });
          _this.state.currentproperty = nextProps.requestResult.properties[0];
          //    alert("else" + JSON.stringify(nextProps.requestResult.properties[0]));

          // alert(this.state.currentproperty.validations);
          if (
            _this.state.currentproperty.validations == undefined ||
            _this.state.currentproperty.validations == null
          ) {
            _this.state.currentproperty = { validations: [] };
            _this.state.currentproperty.validations = [];
          }
          // alert(this.state.currentproperty.validations);
          //_this.setState({ properties: nextProps.requestResult.properties });
          _this.state.properties = nextProps.requestResult.properties;
          //  alert("else" + JSON.stringify(_this.state.properties));
        }

        this.state.currentproperty = { name: "Request", id: this.createID() };
        this.setState({ rootproperty: this.state.currentproperty });
        this.state.currentproperty.childProperties =
          nextProps.requestResult.properties;
        this.state.treeData.push(this.state.currentproperty);
        for (var i = 0; i < nextProps.requestResult.properties.length; i++) {
          if (
            nextProps.requestResult.properties[i].id == undefined ||
            nextProps.requestResult.properties[i].id == "0"
          ) {
            nextProps.requestResult.properties[i].id = this.createID();
          }
        }
      }
    }
  };

  componentWillUnmount() {
    this.setState({ properties: [], treeData: [], currentproperty: [] });
  }
  componentWillRecieve(nextProps) {
    // alert(JSON.stringify(nextProps.requestResult));
    if (nextProps.requestResult.id != this.state.id) {
      if (nextProps.requestResult.id > 0) {
        this.setState({ properties: nextProps.requestResult.properties });
        if (
          nextProps.requestResult.properties == undefined ||
          nextProps.requestResult.properties.length <= 0
        ) {
          nextProps.requestResult.properties = [];
          this.state.currentproperty = { id: this.createID() };
          this.state.currentproperty.name = "Add Property";
          this.state.currentproperty.validations = [];
          nextProps.requestResult.properties.push(this.state.currentproperty);
          this.setState({ name: this.state.currentproperty.name });
        } else {
          this.changestate(
            "id",
            this.state.currentproperty.id,
            this.createID()
          );
          this.changestate("name", this.state.currentproperty.name, "");
          this.changestate("alias", this.state.currentproperty.alias, "");
          this.changestate("type", this.state.currentproperty.type, "");
          this.changestate(
            "description",
            this.state.currentproperty.description,
            ""
          );
          this.changestate(
            "required",
            this.state.currentproperty.required,
            false
          );

          this.setState({
            currentproperty: nextProps.requestResult.properties[0]
          });
          // alert(this.state.currentproperty.validations);
          if (
            this.state.currentproperty.validations == undefined ||
            this.state.currentproperty.validations == null
          ) {
            this.state.currentproperty = { validations: [] };
            this.state.currentproperty.validations = [];
          }
          // alert(this.state.currentproperty.validations);
          this.setState({ properties: nextProps.requestResult.properties });
        }

        this.state.currentproperty = { name: "Request", id: this.createID() };
        this.setState({ rootproperty: this.state.currentproperty });
        this.state.currentproperty.childProperties =
          nextProps.requestResult.properties;
        this.state.treeData.push(this.state.currentproperty);
        for (var i = 0; i < nextProps.requestResult.properties.length; i++) {
          if (
            nextProps.requestResult.properties[i].id == undefined ||
            nextProps.requestResult.properties[i].id == "0"
          ) {
            nextProps.requestResult.properties[i].id = this.createID();
          }
        }
      }
    }
  }

  handleAddChild = property => {
    //alert(property.test);
    //if (property.test != 1) {
    //    property.test = 1;
    //    return;
    //}
    var key = property.node.props.eventKey;
    var property = {};
    property.name = "Add property";
    property.id = this.createID();

    var comp = this.getNode(this.state.properties, key);

    if (comp == undefined) {
      // alert(JSON.stringify(property));
      this.state.rootproperty.childProperties.push(property);
    } else {
      if (comp.childProperties == undefined) {
        comp.childProperties = [];
      }
      comp.childProperties.push(property);
    }

    this.setState({
      selectedKeys: [property.id]
    });
    // property.test = 0;
  };

  changestate(prop, propv, dvalue) {
    if (propv == undefined) {
      propv = dvalue;
    }
    //this.state[prop] = propv;

    if (prop == "required") {
      this.setState({ required: propv });
    } else if (prop == "type") {
      this.setState({ type: propv });
    } else {
      this.state[prop] = propv;
    }
  }

  onExpand = expandedKeys => {
    this.setState({
      expandedKeys,
      autoExpandParent: true
    });
  };

  handleSave = () => {
    //alert(this.state.type);
    this.state.currentproperty.id = this.state.id;
    this.state.currentproperty.name = this.state.name;
    this.state.currentproperty.alias = this.state.alias;
    this.state.currentproperty.description = this.state.description;
    this.state.currentproperty.required = this.state.required;
    this.state.currentproperty.type = this.state.type;
    if (this.state.currentproperty.validations == undefined) {
      this.state.currentproperty.validations = [];
    }
    //alert(this.state.currentproperty.validations);
  };

  handleClose = () => {
    this.state.currentproperty.validations.pop();
    this.setState({ open: false });
    this.setState({ showModal: false });
  };

  getNode = (nodes, id) => {
    for (var i = 0; i < nodes.length; i++) {
      var tnode = nodes[i];
      if (tnode.id == id) return tnode;
      if (
        tnode.childProperties != undefined &&
        tnode.childProperties.length > 0
      ) {
        var cnode = this.getNode(tnode.childProperties, id);
        if (cnode != undefined) return cnode;
      }
    }
  };

  onSelect = (selectedKeys, info) => {
    var searchname = selectedKeys;
    var comp = this.getNode(this.state.properties, searchname);
    // alert(this.state.properties);
    if (comp == undefined) {
      this.setState({
        currentproperty: {},
        id: "",
        name: "",
        alias: "",
        type: 1,
        description: "",
        required: false
      });
      return;
    }

    this.changestate("id", comp.id, this.createID());
    this.changestate("name", comp.name, "");
    this.changestate("alias", comp.alias, "");
    this.changestate("type", comp.type, 1);
    this.changestate("description", comp.description, "");
    this.changestate("required", comp.required, false);
    this.setState({
      currentproperty: comp
    });

    this.setState({
      selectedKeys: [comp.id]
    });
  };
  onClose = () => {
    this.setState({
      visible: false
    });
  };
  handleOk = () => {
    this.setState({
      visible: false
    });
  };
  showModal = () => {
    this.setState({
      expandedKeys: ["0-0-0-key", "0-0-1-key"],
      checkedKeys: ["0-0-0-key"],
      visible: true
    });
    // simulate Ajax
  };
  triggerChecked = () => {
    this.setState({
      checkedKeys: [`0-0-${parseInt(Math.random() * 3, 10)}-key`]
    });
  };
  render() {
    //
    const loop = data => {
      if (data == undefined) return null;
      return data.map(item => {
        if (item.childProperties) {
          return (
            <TreeNode
              key={item.id}
              title={item.name}
              disableCheckbox={item.key === "0-0-0-key"}
            >
              {loop(item.childProperties)}
            </TreeNode>
          );
        }
        return <TreeNode key={item.id} title={item.name} />;
      });
    };

    return (
      <div>
        <div className="col-md-3 col-sm-3 col-xs-3 treeviewdiv">
          <div style={{ padding: "0 20px" }}>
            <Tree
              onExpand={this.onExpand}
              expandedKeys={this.state.expandedKeys}
              onRightClick={this.onRightMenuClick.bind(this)}
              autoExpandParent={true}
              checkedKeys={this.state.checkedKeys}
              onSelect={this.onSelect}
              selectedKeys={this.state.selectedKeys}
              className="treeview"
            >
              {loop(this.state.treeData)}
            </Tree>
          </div>
        </div>
        <div className="col-md-8 col-sm-8 col-xs-8" style={{ paddingTop: 10 }}>
          <form className="form-horizontal form-label-left">
            <div className="form-group">
              <div className="col-md-3 col-sm-12 col-xs-3 keyfield">
                <span className="required">*</span> Name:
              </div>
              <div className="col-md-9 col-sm-12 col-xs-9">
                <input
                  type="hidden"
                  name="id"
                  value={this.state.id}
                  onChange={this.onChange}
                />
                <input
                  type="text"
                  className="form-control"
                  name="name"
                  value={this.state.name}
                  onChange={this.onChange}
                />
              </div>
            </div>

            <div className="form-group">
              <div className="col-md-3 col-sm-12 col-xs-3 keyfield">
                <span className="required">*</span> Alias:
              </div>
              <div className="col-md-9 col-sm-12 col-xs-9">
                <input
                  type="text"
                  className="form-control"
                  name="alias"
                  value={this.state.alias}
                  onChange={this.onChange}
                />
              </div>
            </div>

            <div className="form-group">
              <div className="col-md-3 col-sm-12 col-xs-3 keyfield">
                <span className="required">*</span> Type:
              </div>
              <div className="col-md-9 col-sm-12 col-xs-9">
                <select
                  className="form-control"
                  name="type"
                  value={this.state.type}
                  onChange={this.onChange}
                >
                  <option value="1">Numeric</option>
                  <option value="2">Alpha Numeric</option>
                  <option value="3">Hex Value</option>
                  <option value="4">String</option>
                </select>
              </div>
            </div>

            <div className="form-group">
              <div className="col-md-3 col-sm-12 col-xs-3 keyfield">
                <span className="required">*</span> Description:
              </div>
              <div className="col-md-9 col-sm-12 col-xs-9">
                <textarea
                  className="form-control"
                  name="description"
                  value={this.state.description}
                  onChange={this.onChange}
                />
              </div>
            </div>

            <div className="form-group">
              <div className="col-md-3 col-sm-12 col-xs-3 keyfield">
                <span className="required">*</span> Mandatory:
              </div>
              <div
                style={{ marginTop: 7 }}
                className="col-md-3 col-sm-12 col-xs-9"
              >
                <Checkbox
                  name="required"
                  checked={this.state.required}
                  onChange={this.onCheckedChange}
                  value="checkedMandatory"
                />
              </div>
              <div className="col-md-6" style={{ marginTop: 7 }}>
                <Typography
                  variant="h6"
                  id="modal-title"
                  style={{ fontSize: "15px", textAlign: "left" }}
                >
                  <label style={{ color: "#696969" }}>
                    {" "}
                    Field Validations{" "}
                  </label>
                  &nbsp; &nbsp;
                  <IconButton
                    disableRipple="true"
                    variant="fab"
                    aria-label=" properp"
                    style={Add_icon_style}
                    onClick={() => {
                      this.handleValidate();
                    }}
                  >
                    <AddIcon />
                  </IconButton>
                </Typography>
                {renderValidations(this.props, this)}
                {/* <div>
                                <Typography variant="h6" id="modal-title" style={{ fontSize: '20px' }}>
                                   Test Data
    
                             <IconButton disableRipple="true" variant="fab" aria-label=" properp" style={Add_icon_style} onClick={() => {
                                        this.handleValidate();
                                    }}>
                                        <AddIcon />
                                    </IconButton>

                                </Typography>
                                <EditableTable />
                            </div>*/}
              </div>
            </div>
            <br />
            <div className="col-md-12 col-sm-12 col-xs-3" />

            <div className="form-group buttonDiv">
              <Button type="danger" onClick={this.handleClose}>
                Cancel
              </Button>
              &nbsp;&nbsp;&nbsp;
              <Button onClick={this.handleSave} type="primary">
                Update
              </Button>{" "}
              {/* <Button
                variant="contained"
                onClick={this.handleClose}
                style={Modal_button_style}
              >
                Cancel
              </Button>
              <Button
                variant="contained"
                onClick={this.handleSave}
                style={Color_button_style}
              >
                Update
              </Button> */}
            </div>
          </form>

          <div>
            <Modal
              aria-labelledby="simple-modal-title"
              aria-describedby="simple-modal-description"
              open={this.state.open}
              onClose={this.handleClose}
            >
              {renderValidationModel(this.props, this)}
            </Modal>
          </div>
        </div>
      </div>
    );
  }
}

function renderValidationModel(props, state) {
  return (
    <div style={Modal_style}>
      <Typography variant="h6" id="modal-title" style={{ fontSize: "20px" }}>
        Add Validations{" "}
        <hr style={{ height: "1px", backgroundColor: "black" }} />
      </Typography>
      <Typography
        variant="subtitle1"
        id="simple-modal-description"
        style={{ fontSize: "14px" }}
      >
        <label>Rule</label>
        <br />
        <select
          name="rule"
          value={state.state.rule}
          onChange={state.onVChange}
          className="form-control"
        >
          <option value="1">Equals to</option>
          <option value="2">Less Than</option>
          <option value="3">Less Than Equals to</option>
          <option value="4">Greater Than</option>
          <option value="5">Greater Than Equals to</option>
          <option value="6">Possible Values</option>
        </select>

        <label>Value</label>
        <br />
        <input
          type="number"
          name="value"
          value={state.state.value}
          onChange={state.onVChange}
          className="form-control"
        />

        <label hidden={state.state.groupHide}>Group</label>
        <br />
        <div hidden={state.state.groupHide}>
          <Chips
            value={state.state.group}
            onChange={state.onGroupChange}
            suggestions={["Group1", "Group2", "Group3"]}
          />
        </div>
        <hr style={{ height: "1px", backgroundColor: "black" }} />
      </Typography>

      <Typography id="modal-footer" style={{ fontSize: "14px" }}>
        <Button
          variant="contained"
          onClick={state.handleClose}
          style={Modal_button_style}
        >
          Cancel
        </Button>
        <Button
          variant="contained"
          onClick={state.handleValidationSave}
          style={Color_button_style}
        >
          Save
        </Button>
      </Typography>
    </div>
  );
}
//Working

function renderValidations(props, state) {
  dataJson = state.state.currentproperty.validations;
  //alert("hi" + JSON.stringify(state.state.currentproperty.validations));

  const columns = [
    {
      title: "Validation Name",
      dataIndex: "rule",
      key: "rule"
    },
    {
      title: "Rule",
      dataIndex: "value",
      key: "value"
    },
    {
      title: "",
      dataIndex: "",
      render: record => (
        <center>
          <div>
            <IconButton
              onClick={() => {
                state.handleValidate(record);
              }}
              style={Modal_icon_style}
              aria-label="Edit"
            >
              <EditIcon />
            </IconButton>

            <IconButton
              onClick={() => {
                state.handleDeletion(record);
              }}
              style={Modal_icon_style}
              aria-label="Delete"
            >
              <DeleteIcon />
            </IconButton>
          </div>
        </center>
      )
    }
  ];

  if (
    !(
      state.state.currentproperty.validations == undefined ||
      state.state.currentproperty.validations < 1
    )
  ) {
    return (
      //        <tbody>
      //            {

      //                state.state.currentproperty.validations.map(validation=>
      //                    <tr key={validation.id}>
      //                        <td style={{ width:"60%" }}> {ResolveString(validation.rule)} {validation.value}</td>
      //                        <td>
      //                            <IconButton onClick={() => { state.handleValidate(validation) }}
      //                            style={Modal_icon_style} aria-label="Edit">
      //                            <EditIcon />
      //                        </IconButton>
      //                            <IconButton onClick={() => { state.handleDeletion(validation) }} style={Modal_icon_style} aria-label="Delete">
      //                                <DeleteIcon />
      //                            </IconButton>
      //                        </td>
      //                    </tr>
      //                )
      //            }
      //        </tbody>
      //    );
      //} else {
      //    return (
      //        <tbody>
      //            Please add Validations
      //        </tbody>
      <CompactTable
        dataSource={state.state.currentproperty.validations}
        columns={columns}
      />
    );
  }
}

function ResolveString(value) {
  if (value == 1) return "Equalsto";
  if (value == 2) return "LessThan";
  if (value == 3) return "LessThanEqualsto";
  if (value == 4) return "GreaterThan";
  if (value == 5) return "GreaterThanEqualsto";
}

function ResolveNumber(value) {
  if (value == "Equalsto") return 1;
  if (value == "LessThan") return 2;
  if (value == "LessThanEqualsto") return 3;
  if (value == "GreaterThan") return 4;
  if (value == "GreaterThanEqualsto") return 5;
}

export default connect()(PropertyView);
