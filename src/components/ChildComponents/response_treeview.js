﻿import "rc-tree/assets/index.css";
import React, { Component } from "react";
import { connect } from "react-redux";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import Tree, { TreeNode } from "rc-tree";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import Modal from "@material-ui/core/Modal";
import Typography from "@material-ui/core/Typography";
import AddIcon from "@material-ui/icons/AddCircle";
import Tooltip from "rc-tooltip";
// import Button from "@material-ui/core/Button";
//import  Checkbox  from '@material-ui/core/Checkbox';
import { Checkbox, Button } from "antd";
import "./react-treeview.css";
import "./contextmenu.less";
import CompactTable from "./../../components/table/index";
import { Icon } from "antd";
import NativeListener from "react-native-listener";
import { EditableTable } from "./testdata";

var Modal_style = {
  position: "absolute",
  width: "300px",
  backgroundColor: "#fff",
  boxShadow: "5px",
  padding: "10px 40px",
  margin: "30px 20px 10px 550px",
  borderRadius: "2px"
};
var Modal_button_style = {
  margin: "15px",

  fontSize: "12px"
};
var Color_button_style = {
  margin: "10px",
  fontSize: "12px",
  background: "#1890ff",
  color: "white"
};
var Modal_icon_style = {
  margin: "-5px"
};
var Add_icon_style = {
  margin: "-10px",
  focusVisible: "false",
  //background: '#03a9f4',
  color: "#1890ff"
};
var dataJson = [];

export class ResponseView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      currentproperty: { id: this.createID() },
      //targets: [{ name: 'Root' }],
      currentstateproperty: {},
      responses: [],
      showModal: false,
      id: 0,
      expandedKeys: ["0-0-0-key"],
      autoExpandParent: true,
      checkedKeys: ["0-0-0-key"],
      selectedKeys: [],
      treeData: [],
      currentvalidation: {},
      rootproperty: {},
      isHidden: true,
      isDivHidden: true,
      isButtonHidden: true
    };

    this.mangeTreeviewbusiness(props);
  }

  s4 = () => {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  };

  createID = () => {
    var guid =
      this.s4() +
      this.s4() +
      "-" +
      this.s4() +
      "-" +
      this.s4() +
      "-" +
      this.s4() +
      "-" +
      this.s4() +
      this.s4() +
      this.s4();
    return guid;
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
    this.state.currentproperty[e.target.name] = e.target.value;
  };

  onCheckedChange = e => {
    this.setState({ [e.target.name]: e.target.checked });
    this.state.currentproperty[e.target.name] = e.target.checked;
  };

  onVChange = e => {
    this.setState({ [e.target.name]: e.target.value });
    this.state.currentvalidation[e.target.name] = e.target.value;
  };

  onRightMenuClick = info => {
    if (info.node.props.title == "Response") {
      console.log("hi", info.node.props);
      this.setState({ selectedKeys: [info.node.props.eventKey] });

      this.renderCm(info);
      return false;
    } else {
      return;
    }
  };
  getContainer() {
    if (!this.cmContainer) {
      this.cmContainer = document.createElement("div");
      document.body.appendChild(this.cmContainer);
    }
    return this.cmContainer;
  }

  renderCm(info) {
    if (this.toolTip) {
      ReactDOM.unmountComponentAtNode(this.cmContainer);
      this.toolTip = null;
    }
    this.toolTip = (
      <Tooltip
        trigger="click"
        placement="bottomRight"
        prefixCls="rc-tree-contextmenu"
        defaultVisible
        overlay={
          <div>
            <a onClick={this.handleAddChild(info)}>Add Child</a>
          </div>
        }
      >
        <span />
      </Tooltip>
    );
    const container = this.getContainer();
    Object.assign(this.cmContainer.style, {
      position: "absolute",
      left: `${info.event.pageX}px`,
      top: `${info.event.pageY}px`
    });

    ReactDOM.render(this.toolTip, container);
  }

  handleValidationSave = () => {
    this.state.currentvalidation.rule = ResolveString(this.state.rule);
    this.state.currentvalidation.value = this.state.value;

    this.setState({ open: false });
  };

  mangeTreeviewbusiness = nextProps => {
    if (nextProps.requestResult.id != this.state.id) {
      if (nextProps.requestResult.id > 0) {
        this.setState({ responses: nextProps.requestResult.responses });
        if (
          nextProps.requestResult.responses == undefined ||
          nextProps.requestResult.responses.length <= 0
        ) {
          nextProps.requestResult.responses = [];
          this.state.currentproperty = { id: this.createID() };
          this.state.currentproperty.name = "Status";
          this.state.currentproperty.cstate = "root";
          nextProps.requestResult.responses.push(this.state.currentproperty);
          this.setState({ name: this.state.currentproperty.name });
          //  this.state.responses = nextProps.requestResult.responses;
          // alert(JSON.stringify(nextProps.requestResult.responses));
        } else {
          this.changestate(
            "id",
            this.state.currentproperty.id,
            this.createID()
          );
          this.changestate("name", this.state.currentproperty.name, "");
          this.changestate("cstate", this.state.currentproperty.cstate, "");
          this.changestate("status", this.state.currentproperty.status, "");

          this.changestate(
            "description",
            this.state.currentproperty.description,
            ""
          );

          //this.setState(
          //    {
          //        currentproperty: nextProps.requestResult.responses[0]
          //    });
          this.state.currentproperty = nextProps.requestResult.responses[0];

          // this.setState({ responses: nextProps.requestResult.responses });
          this.state.responses = nextProps.requestResult.responses;
        }

        this.state.currentproperty = { name: "Response", id: this.createID() };
        this.setState({ rootproperty: this.state.currentproperty });
        this.state.currentproperty.childProperties =
          nextProps.requestResult.responses;
        debugger;
        this.state.treeData.push(this.state.currentproperty);
        for (var i = 0; i < nextProps.requestResult.responses.length; i++) {
          if (
            nextProps.requestResult.responses[i].id == undefined ||
            nextProps.requestResult.responses[i].id == "0"
          ) {
            nextProps.requestResult.responses[i].id = this.createID();
          }
        }
      }
    }
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.requestResult.id != this.state.id) {
      this.mangeTreeviewbusiness(nextProps);
    }
    // if (nextProps.requestResult.id != this.state.id) {
    //   if (nextProps.requestResult.id > 0) {
    //     this.setState({ responses: nextProps.requestResult.responses });
    //     if (
    //       nextProps.requestResult.responses == undefined ||
    //       nextProps.requestResult.responses.length <= 0
    //     ) {
    //       nextProps.requestResult.responses = [];
    //       this.state.currentproperty = { id: this.createID() };
    //       this.state.currentproperty.name = "Status";

    //       nextProps.requestResult.responses.push(this.state.currentproperty);
    //       this.setState({ name: this.state.currentproperty.name });
    //     } else {
    //       this.changestate(
    //         "id",
    //         this.state.currentproperty.id,
    //         this.createID()
    //       );
    //       this.changestate("name", this.state.currentproperty.name, "");
    //       this.changestate("cstate", this.state.currentproperty.cstate, "");
    //       this.changestate("status", this.state.currentproperty.status, "");

    //       this.changestate(
    //         "description",
    //         this.state.currentproperty.description,
    //         ""
    //       );

    //       this.setState({
    //         currentproperty: nextProps.requestResult.responses[0]
    //       });

    //       this.setState({ responses: nextProps.requestResult.responses });
    //     }

    //     this.state.currentproperty = { name: "Response", id: this.createID() };
    //     this.setState({ rootproperty: this.state.currentproperty });
    //     this.state.currentproperty.childProperties =
    //       nextProps.requestResult.responses;
    //     this.state.treeData.push(this.state.currentproperty);
    //     for (var i = 0; i < nextProps.requestResult.responses.length; i++) {
    //       if (
    //         nextProps.requestResult.responses[i].id == undefined ||
    //         nextProps.requestResult.responses[i].id == "0"
    //       ) {
    //         nextProps.requestResult.responses[i].id = this.createID();
    //       }
    //     }
    //   }
    // }
  }

  handleAddChild = property => {
    //alert(property.test);
    //if (property.test != 1) {
    //    property.test = 1;
    //    return;
    //}
    var key = property.node.props.eventKey;
    var property = {};
    property.name = "Add property";
    property.id = this.createID();

    var comp = this.getNode(this.state.responses, key);
    // alert(JSON.stringify(comp));
    if (comp == undefined) {
      property.cstate = "root";
      this.state.rootproperty.childProperties.push(property);
    } else {
      if (comp.childProperties == undefined) {
        comp.childProperties = [];
      }
      comp.childProperties.push(property);
    }

    this.setState({
      selectedKeys: [property.id]
    });
    //  property.test = 0;
  };

  changestate(prop, propv, dvalue) {
    if (propv == undefined) {
      propv = dvalue;
    }
    //this.state[prop] = propv;

    if (prop == "required") {
      this.setState({ required: propv });
    } else if (prop == "type") {
      this.setState({ type: propv });
    } else {
      this.state[prop] = propv;
    }
  }

  onExpand = expandedKeys => {
    this.setState({
      expandedKeys,
      autoExpandParent: true,
      isHidden: true
    });
  };

  handleSave = () => {
    this.state.currentproperty.id = this.state.id;
    this.state.currentproperty.status = this.state.status;
    this.state.currentproperty.name = this.state.name;
    this.state.currentproperty.cstate = this.state.cstate;
    this.state.currentproperty.description = this.state.description;
  };

  handleClose = () => {
    // this.state.currentproperty.validations.pop();
    this.setState({ open: false });
    this.setState({ showModal: false });
  };

  getNode = (nodes, id) => {
    for (var i = 0; i < nodes.length; i++) {
      var tnode = nodes[i];
      if (tnode.id == id) return tnode;
      if (
        tnode.childProperties != undefined &&
        tnode.childProperties.length > 0
      ) {
        var cnode = this.getNode(tnode.childProperties, id);
        if (cnode != undefined) return cnode;
      }
    }
  };

  onSelect = (selectedKeys, info) => {
    //  alert("hh" + selectedKeys);
    this.setState({
      isHidden: false
    });
    console.log(info);
    var searchname = selectedKeys;
    var comp = this.getNode(this.state.responses, searchname);
    // alert(JSON.stringify(this.state.responses));
    if (comp == undefined) {
      this.setState({
        currentproperty: {},
        id: "",
        status: "",
        // alias: "",
        name: "",
        description: "",
        isHidden: true,
        isDivHidden: true,
        isButtonHidden: true
      });
      return;
    }
    //if ( == 'Response') {
    //    this.setState({
    //        isHidden: true
    //    });
    //}
    if (comp.cstate == "root") {
      // alert("if");
      this.setState({
        isDivHidden: true,
        isHidden: false,
        isButtonHidden: false
      });
    } else {
      this.setState({
        isDivHidden: false,
        isHidden: true,
        isButtonHidden: false
      });
    }

    this.changestate("id", comp.id, this.createID());
    this.changestate("status", comp.status, "");
    this.changestate("name", comp.name, "");
    this.changestate("cstate", comp.cstate, "");
    this.changestate("description", comp.description, "");

    this.setState({
      currentproperty: comp
    });

    this.setState({
      selectedKeys: [comp.id]
    });
  };
  onClose = () => {
    this.setState({
      visible: false
    });
  };
  handleOk = () => {
    this.setState({
      visible: false
    });
  };
  showModal = () => {
    this.setState({
      expandedKeys: ["0-0-0-key", "0-0-1-key"],
      checkedKeys: ["0-0-0-key"],
      visible: true
    });
    // simulate Ajax
  };
  triggerChecked = () => {
    this.setState({
      checkedKeys: [`0-0-${parseInt(Math.random() * 3, 10)}-key`]
    });
  };
  render() {
    const loop = data => {
      if (data == undefined) return null;
      //alert(JSON.stringify(data));
      return data.map(item => {
        //  alert(JSON.stringify(item));
        if (item.cstate == "root") {
          var titlevalue = item.status;
        } else {
          var titlevalue = item.name;
        }
        if (item.childProperties) {
          //  { item.cstate == 'root' ?  title = { item.alias } : title = { item.name } }
          return (
            <TreeNode
              key={item.id}
              title={titlevalue}
              disableCheckbox={item.key === "0-0-0-key"}
            >
              {loop(item.childProperties)}
            </TreeNode>
          );
        }
        return <TreeNode key={item.id} title={titlevalue} />;
      });
    };

    return (
      <div className="col-md-12 col-sm-12 col-xs-3">
        <div className="col-md-3 col-sm-3 col-xs-3 treeviewdiv">
          <div style={{ padding: "0 20px" }}>
            <Tree
              onExpand={this.onExpand}
              expandedKeys={this.state.expandedKeys}
              onRightClick={this.onRightMenuClick.bind(this)}
              autoExpandParent={true}
              checkedKeys={this.state.checkedKeys}
              onSelect={this.onSelect}
              selectedKeys={this.state.selectedKeys}
              className="treeview"
            >
              {loop(this.state.treeData)}
            </Tree>
          </div>
        </div>
        <div className="col-md-8 col-sm-8 col-xs-8">
          <form className="form-horizontal form-label-left">
            <div className="form-group">
              <div
                hidden={this.state.isHidden}
                className="col-md-3 col-sm-12 col-xs-3"
              >
                <span className="required">*</span> Status:
              </div>
              <div className="col-md-8 col-sm-8 col-xs-8">
                {/*  <input type="hidden" name="id" value={this.state.id} onChange={this.onChange} />
                                <input type="text" className="form-control" name="name" value={this.state.status}
                                    onChange={this.onChange} />*/}
                <select
                  className="form-control"
                  name="status"
                  value={this.state.status}
                  onChange={this.onChange}
                  hidden={this.state.isHidden}
                >
                  <option value="100">100-Continue</option>
                  <option value="101">101-Switching Protocol</option>
                  <option value="102">102-Processing</option>
                  <option value="200">200-OK</option>
                  <option value="201">201-Created</option>
                  <option value="204">204-No Content</option>
                  <option value="400">400-Bad Request</option>
                  <option value="404">404-Not Found</option>
                </select>
              </div>
            </div>

            <div className="form-group" hidden={this.state.isDivHidden}>
              <div className="col-md-3 col-sm-12 col-xs-3 keyfield">
                <span className="required">*</span> Name:
              </div>
              <div className="col-md-9 col-sm-12 col-xs-9">
                <input
                  type="text"
                  className="form-control"
                  name="name"
                  value={this.state.name}
                  onChange={this.onChange}
                />
              </div>
            </div>

            <div className="form-group" hidden={this.state.isDivHidden}>
              <div className="col-md-3 col-sm-12 col-xs-3 keyfield">
                <span className="required">*</span> Description:
              </div>
              <div className="col-md-9 col-sm-12 col-xs-9">
                <textarea
                  className="form-control"
                  name="description"
                  value={this.state.description}
                  onChange={this.onChange}
                />
              </div>
            </div>

            <div
              className="form-group buttonDiv"
              hidden={this.state.isButtonHidden}
            >
              <Button type="danger" onClick={this.handleClose}>
                Cancel
              </Button>
              &nbsp;&nbsp;&nbsp;
              <Button onClick={this.handleSave} type="primary">
                Update
              </Button>{" "}
              {/* <Button
                variant="contained"
                onClick={this.handleClose}
                style={Modal_button_style}
              >
                Cancel
              </Button>
              <Button
                variant="contained"
                onClick={this.handleSave}
                style={Color_button_style}
              >
                Update
              </Button> */}
            </div>
          </form>

          <div>
            <Modal
              aria-labelledby="simple-modal-title"
              aria-describedby="simple-modal-description"
              open={this.state.open}
              onClose={this.handleClose}
            >
              {renderValidationModel(this.props, this)}
            </Modal>
          </div>
        </div>
      </div>
    );
  }
}

function renderValidationModel(props, state) {
  return (
    <div style={Modal_style}>
      <Typography variant="h6" id="modal-title" style={{ fontSize: "20px" }}>
        Add Validations{" "}
        <hr style={{ height: "1px", backgroundColor: "black" }} />
      </Typography>
      <Typography
        variant="subtitle1"
        id="simple-modal-description"
        style={{ fontSize: "14px" }}
      >
        <label>Rule</label>
        <br />
        <select
          name="rule"
          value={state.state.rule}
          onChange={state.onVChange}
          className="form-control"
        >
          <option value="1">Equals to</option>
          <option value="2">Less Than</option>
          <option value="3">Less Than Equals to</option>
          <option value="4">Greater Than</option>
          <option value="5">Greater Than Equals to</option>
        </select>

        <label>Value</label>
        <br />
        <input
          type="number"
          name="value"
          value={state.state.value}
          onChange={state.onVChange}
          className="form-control"
        />
        <hr style={{ height: "1px", backgroundColor: "black" }} />
      </Typography>

      <Typography id="modal-footer" style={{ fontSize: "14px" }}>
        <Button
          variant="contained"
          onClick={state.handleClose}
          style={Modal_button_style}
        >
          Cancel
        </Button>
        <Button
          variant="contained"
          onClick={state.handleValidationSave}
          style={Color_button_style}
        >
          Save
        </Button>
      </Typography>
    </div>
  );
}
//Working

function ResolveString(value) {
  if (value == 1) return "Equals to";
  if (value == 2) return "Less Than";
  if (value == 3) return "Less Than Equals to";
  if (value == 4) return "Greater Than";
  if (value == 5) return "Greater Than Equals to";
}

export default connect()(ResponseView);
