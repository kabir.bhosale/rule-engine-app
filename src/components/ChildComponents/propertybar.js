﻿import React, { Component } from "react";
import { connect } from "react-redux";
import { HeaderBar } from "./header";
import { PropertyView } from "./treeview";
import { ResponseView } from "./response_treeview";

import ReactDOM from "react-dom";
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";

import { withStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Modal from "@material-ui/core/Modal";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import Icon from "@material-ui/core/Icon";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";
import AddIcon from "@material-ui/icons/AddCircle";
import { TestBar } from "./testdata";
import { Tabs, Card } from "antd";

var ExpansionPanel_style = {
  fontSize: "18px"
  // fontWeight: fontWeightRegular
};
var vertical_line_style = {
  borderLeft: "1px solid black",
  padding: "1px 50px 1px 35px",
  marginLeft: "150px"
};
var Property_button_style = {
  fontSize: "12px",
  marginLeft: "300px"
};
var Property_Savebutton_style = {
  margin: "10px",
  fontSize: "15px",
  backgroundColor: "white",
  border: "1px solid #f50057",
  padding: "8px",
  borderRadius: "3px",
  width: "75px"
};
var Tree_hover_style = {
  "&: hover": {
    backgroundColor: "#ff80ab",
    cursor: "pointer"
  }
};

var handleOpen = () => {
  this.setState({ open: true });
};

var Add_icon_style = {
  margin: "-10px",
  focusVisible: "false"
};

const TabPane = Tabs.TabPane;

export class PropertyBar extends Component {
  //  <div id="sidebar-menu" className="main_menu_side hidden-print main_menu">
  //    <div className="x_panel">
  //        <div className="x_title">
  //            <div className="x_content">
  //                <div id="sidebar-menu" className="main_menu_side hidden-print main_menu">
  //                    <ExpansionPanel>
  //                        <ExpansionPanelSummary style={{ backgroundColor: '#DCDCDC' }} expandIcon={<ExpandMoreIcon onClick={() => {} } />}>
  //                            <Typography style={ExpansionPanel_style}>Headers</Typography>
  //                        </ExpansionPanelSummary>

  //                        <ExpansionPanelDetails>
  //                            <HeaderBar {...props} />
  //                        </ExpansionPanelDetails>
  //                    </ExpansionPanel>
  //                </div>
  //            </div>
  //        </div>
  //    </div>

  //    <div className="clearfix" />

  //    <div> &nbsp; </div>

  //    <div className="x_panel">
  //        <div className="x_title">
  //            <div className="x_content">
  //                <div id="sidebar-menu" className="main_menu_side hidden-print main_menu">
  //                    <ExpansionPanel>
  //                        <ExpansionPanelSummary style={{ backgroundColor: '#DCDCDC' }} expandIcon={<ExpandMoreIcon onClick={() => { }} />}>
  //                            <Typography style={ExpansionPanel_style}>Request Body</Typography>
  //                        </ExpansionPanelSummary>

  //                        <ExpansionPanelDetails>
  //                            <PropertyView {...props} />
  //                        </ExpansionPanelDetails>
  //                    </ExpansionPanel>
  //                </div>
  //            </div>
  //        </div>
  //    </div>

  //    <div> &nbsp; </div>

  //    <div className="x_panel">
  //        <div className="x_title">
  //            <div className="x_content">
  //                <div id="sidebar-menu" className="main_menu_side hidden-print main_menu">
  //                    <ExpansionPanel>
  //                        <ExpansionPanelSummary style={{ backgroundColor: '#DCDCDC' }} expandIcon={<ExpandMoreIcon onClick={() => { }} />}>
  //                            <Typography style={ExpansionPanel_style}>Response Body</Typography>
  //                        </ExpansionPanelSummary>

  //                        <ExpansionPanelDetails>
  //                            <ResponseView {...props} />
  //                        </ExpansionPanelDetails>
  //                    </ExpansionPanel>
  //                </div>
  //            </div>
  //        </div>
  //    </div>

  //    <div> &nbsp; </div>

  //    <div className="x_panel">
  //        <div className="x_title">
  //            <div className="x_content">
  //                <div id="sidebar-menu" className="main_menu_side hidden-print main_menu">
  //                    <ExpansionPanel>
  //                        <ExpansionPanelSummary style={{ backgroundColor: '#DCDCDC' }} expandIcon={<ExpandMoreIcon onClick={() => { }} />}>
  //                            <Typography style={ExpansionPanel_style}>Test Data</Typography>
  //                        </ExpansionPanelSummary>

  //                        <ExpansionPanelDetails>
  //                            <TestBar {...props} />
  //                        </ExpansionPanelDetails>
  //                    </ExpansionPanel>
  //                </div>
  //            </div>
  //        </div>
  //    </div>

  //</div>  */
  render() {
    // console.log(JSON.stringify(this.props));
    return (
      <div style={{ paddingTop: 10 }}>
        <Card title="Properties">
          <Tabs type="card">
            <TabPane tab="Headers" key="1">
              <HeaderBar {...this.props} />
            </TabPane>
            <TabPane tab="Request Body" key="2">
              <PropertyView {...this.props} />
            </TabPane>
            <TabPane tab="Response Body" key="3">
              {" "}
              <ResponseView {...this.props} />
            </TabPane>
            <TabPane tab="Test Data" key="4">
              {" "}
              <TestBar {...this.props} />
            </TabPane>
          </Tabs>
        </Card>
      </div>
      //   <div>
      //     <Tabs>
      //       <TabPane tab="Headers" key="1">
      //         <HeaderBar {...this.props} />
      //       </TabPane>
      //       <TabPane tab="Request Body" key="2">
      //         <PropertyView {...this.props} />
      //       </TabPane>
      //       <TabPane tab="Response Body" key="3">
      //         {" "}
      //         <ResponseView {...this.props} />
      //       </TabPane>
      //       <TabPane tab="Test Data" key="4">
      //         {" "}
      //         <TestBar {...this.props} />
      //       </TabPane>
      //     </Tabs>
      //   </div>
    );
  }
}

export default connect()(PropertyBar);
