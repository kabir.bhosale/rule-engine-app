﻿import React, { Component } from "react";
import CreateReactClass from "create-react-class";
import { connect } from "react-redux";

import ReactDOM from "react-dom";
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";

import { withStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Modal from "@material-ui/core/Modal";
import Button from "@material-ui/core/Button";
//import Checkbox from '@material-ui/core/Checkbox';
import { Checkbox } from "antd";
import Icon from "@material-ui/core/Icon";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";
import AddIcon from "@material-ui/icons/AddCircle";

var Modal_style = {
  position: "absolute",
  width: "300px",
  backgroundColor: "#fff",
  boxShadow: "5px",
  padding: "10px 40px",
  margin: "30px 20px 10px 550px",
  borderRadius: "2px"
};
var Modal_button_style = {
  margin: "15px",
  fontSize: "12px"
};
var Color_button_style = {
  margin: "10px",
  fontSize: "12px",
  background: "#1890ff",
  color: "white"
};
var Modal_icon_style = {
  margin: "-5px"
};
var Add_icon_style = {
  margin: "-10px",
  focusVisible: "false",
  color: "#1890ff"
};

var ExpansionPanel_style = {
  fontSize: "14px"
  // fontWeight: fontWeightRegular
};
var outData = [];
export class TestBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      currentHeader: {},
      changeheader: 1,
      newData: []
    };
  }

  handleDelete = currentHeader => {
    if (currentHeader == undefined) {
      //  currentHeader = {};
      //this.props.requestResult.testData.push(currentHeader);
    }
    // alert(JSON.stringify(currentHeader));
    //  alert(this.props.requestResult.testData.indexOf(currentHeader));
    this.props.requestResult.testData.splice(
      this.props.requestResult.testData.indexOf(currentHeader),
      1
    );
    //this.setState({ open: true, headerid: currentHeader.id, headerkey: currentHeader.key, headervalue: currentHeader.value, headermethod: currentHeader.method, headerrequired: currentHeader.required });
    this.setState({ currentHeader: currentHeader });
  };

  onChange = e => {
    //alert(e.target.name);
    this.setState({ [e.target.name]: e.target.value });
    this.state.currentHeader[e.target.name] = e.target.value;
  };

  onCheckedChange = e => {
    this.setState({ [e.target.name]: e.target.checked });
    this.state.currentHeader[e.target.name] = e.target.checked;
  };

  handleOpen = currentHeader => {
    // alert(JSON.stringify(currentHeader));
    if (this.props.requestResult.testData == null) {
      this.props.requestResult.testData = [];
    }
    if (currentHeader == undefined) {
      currentHeader = {};

      this.props.requestResult.testData.push(currentHeader);
    }
    this.setState({
      open: true,
      headerid: currentHeader.id
    });
    for (var i = 0; i < outData.length; i++) {
      var name = outData[i];

      this.setState({ [`${name}`]: currentHeader[`${name}`] });
    }

    // this.setState({ open: true, headerid: currentHeader.id, headerkey: currentHeader.key, headervalue: currentHeader.value, headermethod: currentHeader.method, headerrequired: currentHeader.required });
    this.setState({ open: true, currentHeader: currentHeader });
  };

  handleSave = () => {
    this.state.currentHeader.id = this.state.headerid;
    for (var i = 0; i < outData.length; i++) {
      var name = outData[i];
      // alert(this.state.name);
      this.state.currentHeader.name = this.state[`${name}`];
    }
    // this.state.currentHeader.Card = this.state.Card;
    // alert(JSON.stringify(this.state.currentHeader));
    this.setState({ open: false });
  };
  handleClose = () => {
    this.props.requestResult.testData.pop();
    this.setState({ open: false });
  };

  render() {
    return (
      <div className="col-md-12 col-sm-12 col-xs-3">
        {renderHeaders(this.props, this)}
        <div className="col-md-3 col-sm-3 col-xs-12">
          <Modal
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={this.state.open}
            onClose={() => {
              this.state.setState({ open: false });
            }}
          >
            <div style={Modal_style}>
              <Typography
                variant="h6"
                id="modal-title"
                style={{ fontSize: "20px" }}
              >
                Add Header{" "}
                <hr style={{ height: "1px", backgroundColor: "black" }} />
              </Typography>
              {outData.map(name => (
                // const tmpname = name;
                <Typography
                  variant="subtitle1"
                  id="simple-modal-description"
                  style={ExpansionPanel_style}
                >
                  <label className="control-label">{name}</label>
                  <input
                    type="hidden"
                    name="headerid"
                    value={this.state.headerid}
                    onChange={this.onChange}
                    className="form-control"
                  />
                  <input
                    type="text"
                    name={name}
                    value={this.state[`${name}`]}
                    onChange={this.onChange}
                    className="form-control"
                  />
                </Typography>
              ))}
              <Typography id="modal-footer" style={ExpansionPanel_style}>
                <hr style={{ height: "1px", backgroundColor: "black" }} />
                <Button
                  variant="contained"
                  onClick={this.handleClose}
                  style={Modal_button_style}
                >
                  Cancel
                </Button>
                <Button
                  variant="contained"
                  onClick={this.handleSave}
                  style={Color_button_style}
                >
                  Save
                </Button>
              </Typography>
            </div>
          </Modal>
        </div>
      </div>
    );
  }
}

//export const HeaderBar = props => (
//    <div className="col-md-12 col-sm-12 col-xs-3" >
//        {renderHeaders(props)}
//        {renderPopUp(props)}
//    </div>
//);

function renderHeaders(props, state) {
  var newData1 = [];
  //alert(JSON.stringify(props.requestResult.properties));
  if (
    !(
      props.requestResult.properties == undefined ||
      props.requestResult.properties.length < 1
    )
  ) {
    var data = props.requestResult;
    //alert("hi" + JSON.stringify(props.requestResult.properties));
    console.log("data", typeof data);
    console.log("dataLength", data.properties.length);
    // console.log(JSON.stringify(props.requestResult));
    for (var i = 0; i < data.properties.length; i++) {
      newData1.push(data["properties"][i]["name"]);
      //alert((data["properties"][i]["childProperties"]));
      if (
        !(
          JSON.stringify(data["properties"][i]["childProperties"]) ==
            undefined || data["properties"][i]["childProperties"] < 1
        )
      ) {
        var str1 = data["properties"][i]["name"];
        for (
          var j = 0;
          j < data["properties"][i]["childProperties"].length;
          j++
        ) {
          var str2 = data["properties"][i]["childProperties"][j]["name"];
          var newStr = str1 + "." + str2;
          newData1.push(newStr);
        }
      } else {
        console.log("Empty Child");
      }
    }
    outData = newData1;
    //state.setState({ newData: newData1 });
  }
  console.log(newData1);

  if (
    !(
      props.requestResult.properties == undefined ||
      props.requestResult.properties.length < 1
    )
  ) {
    return (
      <table
        className="table table-bordered center"
        style={{ paddingTop: 10, textAlign: "center" }}
      >
        <thead>
          <tr>
            {newData1.map(name => (
              <th>{name}</th>
            ))}
            <th>
              {" "}
              <IconButton
                disableRipple="true"
                variant="fab"
                color="primary"
                aria-label=" properp"
                style={Add_icon_style}
                onClick={() => {
                  state.handleOpen();
                }}
              >
                <AddIcon />
              </IconButton>
            </th>
          </tr>
        </thead>
        {renderBody(props, state)}
      </table>
    );
  }
}

function renderBody(props, state) {
  //alert(JSON.stringify(props.requestResult));
  if (
    !(
      props.requestResult.testData == undefined ||
      props.requestResult.testData.length < 1
    )
  ) {
    return (
      <tbody>
        {props.requestResult.testData.map(header => (
          <tr key={header.id}>
            {outData.map(name => (
              <td>{header[`${name}`]}</td>
            ))}
            <td>
              <IconButton
                onClick={() => {
                  state.handleOpen(header);
                }}
                style={Modal_icon_style}
                aria-label="Edit"
              >
                <EditIcon />
              </IconButton>
              <IconButton
                onClick={() => {
                  state.handleDelete(header);
                }}
                style={Modal_icon_style}
                aria-label="Delete"
              >
                <DeleteIcon />
              </IconButton>
            </td>
          </tr>
        ))}
      </tbody>
    );
  } else {
    return <tbody>Please add header</tbody>;
  }
}

export default connect()(TestBar);
