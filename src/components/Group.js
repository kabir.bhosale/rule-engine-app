﻿import React, { Component } from "react";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import CompactTable from "./../components/table/index";
import Typography from "@material-ui/core/Typography";
import AddIcon from "@material-ui/icons/AddCircle";
import AddNewGroup from "./modal-forms/AddGroup";
import Modal from "@material-ui/core/Modal";

var Modal_style = {
  position: "absolute",
  width: "300px",
  backgroundColor: "#fff",
  boxShadow: "5px",
  padding: "10px 40px",
  margin: "30px 20px 10px 550px",
  borderRadius: "2px"
};
var Modal_button_style = {
  margin: "10px",
  fontSize: "12px"
};
var Color_button_style = {
  margin: "10px",
  fontSize: "12px",
  background: "#1890ff",
  color: "white"
};
var Modal_icon_style = {
  margin: "-5px"
};
var Add_icon_style = {
  margin: "-10px",
  focusVisible: "false",
  //background: '#03a9f4',
  color: "#1890ff"
};

class Group extends Component {
  constructor(props) {
    super(props);
    this.state = {
      groupRecords: [],
      currentGroup: {},
      groupData: {},
      showModal: false,
      open: false
    };

    this.addGroupFormRef = {};

    this.saveFormRef = this.saveFormRef.bind(this);
    this.handleGroupSave = this.handleGroupSave.bind(this);
    this.handleGroupCancel = this.handleGroupCancel.bind(this);
    this.handleShowAddModal = this.handleShowAddModal.bind(this);

    this.getGroup();
  }

  saveFormRef(formRef) {
    if (formRef) {
      this.addGroupFormRef = formRef.props.form;
    }
  }

  handleShowAddModal() {
    this.setState({
      visible: true
    });
  }

  handleGroupSave() {
    this.addGroupFormRef.validateFields((err, values) => {
      if (!err) {
        fetch(`http://10.10.18.13:6858/api/Requests/SaveGroup`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            Group_Name: values.groupName,
            rule_desc: values.groupDesc
          })
        })
          .then(response => {
            response.json().then(body => {
              this.addGroupFormRef.resetFields();
              this.handleGroupCancel();
              this.getGroup();
            });
          })
          .catch(err => err);
      }
    });
  }

  handleGroupCancel() {
    this.addGroupFormRef.resetFields();
    this.setState({
      visible: false
    });
  }

  componentWillReceiveProps() {
    this.getGroup();
  }

  getGroup = () => {
    fetch(`http://10.10.18.13:6858/api/Requests/GetGroups`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
        //   'Access-Control-Allow-Origin':'*'
      }
    })
      .then(response => {
        if (response == undefined) return {};
        return response.json();
      })
      .then(response => {
        // alert("Get groupData" + JSON.stringify(response));
        console.log("GROUPS :: ", response);
        this.setState({ groupRecords: response });
      })
      .catch(err => {
        // alert("Error");
      });
  };

  handleValidate = validator => {
    alert("Validator" + JSON.stringify(validator));
    if (validator == undefined) {
      validator = {};
      //if (this.state.currentproperty.validations == undefined || this.state.currentproperty.validations == null) {
      //    this.state.currentproperty.validations = [];
      //}
      this.state.currentGroup.push(validator);
    }

    this.setState({
      groupData: validator,
      group_Name: validator.group_Name,
      rule_desc: validator.rule_desc
    });
    this.setState({ showModal: true });
    this.setState({ open: true });
  };

  onChange = e => {
    //alert(e.target.name);
    this.setState({ [e.target.name]: e.target.value });
    this.state.currentGroup[e.target.name] = e.target.value;
    // alert("groupData" + JSON.stringify(this.state.groupData));
  };

  onVChange = e => {
    //alert(e.target.name);
    this.setState({ [e.target.name]: e.target.value });
    this.state.groupData[e.target.name] = e.target.value;
    // alert("groupData" + JSON.stringify(this.state.groupData));
  };

  handleClose = () => {
    //alert(JSON.stringify(this.prop.requestResult));
  };

  handleValidationSave = () => {
    //alert(this.state.rule);
    this.state.groupData.group_Name = this.state.group_Name;
    this.state.groupData.rule_desc = this.state.rule_desc;
    alert(JSON.stringify(this.state.groupData));

    this.setState({ open: false });

    fetch(`http://10.10.18.13:6858/api/Requests/SaveGroup`, {
      method: "POST",
      //  mode: 'no-cors',
      headers: {
        "Content-Type": "application/json"
        //   'Access-Control-Allow-Origin':'*'
      },
      body: JSON.stringify({
        id: JSON.stringify(this.state.groupData.id),
        Group_Name: JSON.stringify(this.state.groupData.group_Name),
        rule_desc: JSON.stringify(this.state.groupData.rule_desc)
      })
    })
      .then(response => {
        console.log("saved successfully", response);
        response.json().then(body => {
          var JSONResponse = body;
          console.log("JSONResponse", JSONResponse);
        });
      })
      .catch(err => err);
  };

  handleSave = () => {
    //  this.state.currentGroup.push(this.state.currentGroup);
    alert(JSON.stringify(this.state.currentGroup));
    fetch(`http://10.10.18.13:6858/api/Requests/SaveGroup`, {
      method: "POST",
      //  mode: 'no-cors',
      headers: {
        "Content-Type": "application/json"
        //   'Access-Control-Allow-Origin':'*'
      },
      body: JSON.stringify({
        id: this.getUrlParameter("id"),
        Group_Name: JSON.stringify(this.state.currentGroup.group_Name),
        rule_desc: JSON.stringify(this.state.currentGroup.rule_desc)
      })
    })
      .then(response => {
        console.log("saved successfully", response);
        response.json().then(body => {
          var JSONResponse = body;
          console.log("JSONResponse", JSONResponse);
        });
      })
      .catch(err => err);
  };

  //getUrlParameter(name) {
  //    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  //    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
  //    var results = regex.exec(location.search);
  //    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
  //};

  render() {
    const columns = [
      {
        title: "Group Name",
        dataIndex: "group_Name",
        key: "group_Name"
      },
      {
        title: "Group Description",
        dataIndex: "rule_desc",
        key: "rule_desc"
      },

      {
        title: "",
        dataIndex: "",
        render: (text, record) =>
          this.state.groupRecords.length >= 1 ? (
            <center>
              <div>
                <IconButton style={Modal_icon_style} aria-label="Edit">
                  <EditIcon />
                </IconButton>
                <IconButton style={Modal_icon_style} aria-label="Delete">
                  <DeleteIcon />
                </IconButton>
              </div>
            </center>
          ) : null
      }
    ];

    return (
      <div>
        <div>
          <form>
            <div className="form-group">
              <h2 className="col-sm-3" style={{ marginTop: "15px" }}>
                Groups
              </h2>
              <div className="col-sm-3" style={{ float: "right" }}>
                <Button
                  variant="contained"
                  onClick={this.handleClose}
                  style={Modal_button_style}
                >
                  Cancel
                </Button>
                <Button
                  variant="contained"
                  onClick={this.handleSave}
                  style={Color_button_style}
                >
                  Save
                </Button>
              </div>
            </div>
            <br />
            <div className="col-md-12 col-sm-12 col-xs-3">
              <Typography
                variant="h6"
                id="modal-title"
                style={{ fontSize: "14px", textAlign: "left" }}
              >
                <br />
                <label className="form-group" style={{ color: "#696969" }}>
                  {" "}
                  Group Table{" "}
                </label>
                &nbsp; &nbsp;
                <IconButton
                  disableRipple="true"
                  variant="fab"
                  aria-label=" properp"
                  style={Add_icon_style}
                  onClick={() => {
                    this.handleShowAddModal();
                  }}
                >
                  <AddIcon />
                </IconButton>
              </Typography>
              <CompactTable
                dataSource={this.state.groupRecords}
                columns={columns}
              />
            </div>
          </form>

          <div>
            <AddNewGroup
              wrappedComponentRef={this.saveFormRef}
              visible={this.state.visible}
              onCreate={this.handleGroupSave}
              onCancel={this.handleGroupCancel}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default connect()(Group);
