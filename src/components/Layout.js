import React from "react";
import { Col, Grid, Row } from "react-bootstrap";
import NavMenu from "./NavMenu";
import { Menu, Icon, Switch } from "antd";

export default props => (
  <Grid fluid>
    <Col sm={3}>
      <NavMenu />
    </Col>
    <Col sm={9}>{props.children}</Col>
  </Grid>
);

export const HideNavLayout = props => (
  <Grid fluid>
    <Row>
      <Col sm={3} />
      <Col sm={9}>{props.children}</Col>
    </Row>
  </Grid>
);
