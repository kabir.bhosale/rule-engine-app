import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Select, Steps, Form, Row, Col, Card} from 'antd';
const Option = Select.Option;
// import Img from './../../assets/images/flowDig.png';
// //import './ReconFlow.less'
// import ReconProcess1 from './../modal-forms/ReconProcess1';
// import ReconProcess2 from './../modal-forms/ReconProcess2';
// import TabModal from './TabModal';

const FormItem = Form.Item;





  

  const postURL = "http://10.11.14.79:8081/recon/product/save/";
  const NewSourceDefinition = Form.create()(
    class extends React.Component {
//class NewSourceDefinition extends Component {

    constructor(props){
        super(props)
  
        this.handleChange = this.handleChange.bind(this);
      //  this.onCancel
      }


     handleChange(value) {
        console.log(`selected ${value}`);

        this.props.setForm(this.props.form)
    
    }
  
    //-------------------------------------------
    setVal(values) {
        
        //using simple fetch
        fetch(postURL, {
            method: 'POST',
          //  mode: 'no-cors',
            headers: {
                'Content-Type': 'application/json',
              //   'Access-Control-Allow-Origin':'*'
            },
            body: JSON.stringify({

                productName: values.title,
                productDescription: values.description,
                productId: values.ProjectID,
                documentType: values.document_type,
                headerPresent: values.headerSet

            })
            }).then(response => {
                console.log('saved successfully', response);
                if (response.status == 200) {
                    setTimeout(() => this.setState({ loadervisible: false }), 800);
                    // this.setState({loadervisible: false});
                    this.getProductData();
                }
            }).catch(err => err);

    }

    saveSourceForm = (formRef) => { 
        this.saveSourceForm = formRef;

        console.log("SOURCE DATA ::: ")

         }

    
    //--------------------------------------------
    render(){

        const { visible, onCancel, onCreate, form, visible1, onCancel1, onCreate1, } = this.props;
       
        console.log("SOURCE FORM :: ",form);
        
       
        const { getFieldDecorator } = form;

        return(
            // <Form>
            // <FormItem className="formLabels1" label="Select Source:">
            //   {(

                    
            //   )}
            // </FormItem> 
            // </Form>
            <Card className="cardStyle">

            <Row>
            <Col span={6}>
                <label className="subHeading3">Input Format :</label>
            </Col>
            <Col span={8}>

            <Form layout="vertical">

             <FormItem  >
                {getFieldDecorator('dataType', {
                  rules: [{ required: true, message: 'Please Select Source Type !' }],
                })
                (
                  // <div className="dropdown"> 
                  <Select defaultValue="Select Source Type" className="selectElement" onChange={this.handleChange} placeholder="Select File Format">
                     <Option value="csv">Delimited</Option>
                     <Option value="xls">XLS</Option>
                     <Option value="fix">Fixed Length</Option>
                     <Option value="database">Database</Option>
                   </Select>
                  // </div>
                )}
              </FormItem> 

            </Form>  
            </Col>
            </Row>

            </Card>
        )
    }
});

export default NewSourceDefinition;