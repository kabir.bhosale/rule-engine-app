import * as React from 'react';
import TabModal from '../recon-flow/TabModal';
import { StencilService } from './services/stencil-service';
import { ToolbarService } from './services/toolbar-service';
import { InspectorService } from './services/inspector-service';
import { HaloService } from './services/halo-service';
import { KeyboardService } from './services/keyboard-service';
import RappidService from './services/kitchensink-service';
import AddNewRecord from '../modal-forms/AddNewRecord'
import ReconProcess1 from '../modal-forms/ReconProcess1';
import ReconProcess2 from '../modal-forms/ReconProcess2';
import ReconProcess3 from "../modal-forms/ReconProcess3";
//import { Link } from "react-router-dom";
//import { Modal } from 'antd';
import { message, Upload, Popover, Icon } from "antd";
import Button from '@material-ui/core/Button';
import "../css/mainRappid.less";
import CSVDialogBox from '../recon-flow/CSVDialogBox';
import SourceLoader from "../modal-forms/SourceLoader";
//import { withRouter } from 'react-router-dom';
import { LinkContainer } from 'react-router-bootstrap';

//var hoverContent = require('./../infomapper.js').default; 
//const urls = require("./../../utility/urls").default;
// const TabModal = require('../recon-flow/TabModal')
interface Props {
    openProject: any,
    id: any
    //name: any,
    //location: any
}

interface State {
    visible: any,
    openProject: any,
    addRecordVisible: boolean,
    sourcedef: boolean,
    activeTab: any,
    loadervisible: boolean,
    id: any,
    //new
    isOpen: any,
    queryData: any,
    dataSource:any
    // getReqObject : any,
    // tokenizerData : any,
    //autoConfigDisable: boolean
}

class Rappid extends React.Component<Props, State> {
    currentTab: any;
    rappid: RappidService;
    formRef: any;
    //saveSourceForm: any;

    //reqObject: any;
    //stepConfig: any;
    // sourceConfig :any;
    // fileTypeSourceConfig  :any;
    //fieldConfig: any;
    // delimittedFileConfig :any;

    constructor(props: Props) {
        super(props);
        this.state = {
            visible: '',
            addRecordVisible: false,
            activeTab: "1",
            loadervisible: false,
            sourcedef: false,
            openProject: "",
            isOpen: false,
            id: "",
            queryData: [],
            dataSource: []
        };

        // this.handleSave = this.handleSave.bind(this);
        this.handleOk = this.handleOk.bind(this);
        // this.setData = this.setData.bind(this)
        this.setCurrentTab = this.setCurrentTab.bind(this);
        this.goBack = this.goBack.bind(this);
        //this.handleUpload = this.handleUpload.bind(this);
        //this.uploadingFlowData = this.uploadingFlowData.bind(this);
        //this.handleCSVCancel = this.handleCSVCancel.bind(this);
        // this.handleCSVOk = this.handleCSVOk.bind(this);
        // this.getProjectInfo = this.getProjectInfo.bind(this);
        //this.saveWorkFlowData = this.saveWorkFlowData.bind(this);
        //this.onApply = this.onApply.bind(this);
        //this.onUploadChange = this.onUploadChange.bind(this); //changed by Divyansh
        //this.uploadingFlowData(props);
        //this.reqObject = {};
        //this.stepConfig = {};
        // this.sourceConfig = {};
        // this.fileTypeSourceConfig = {};
        // this.fieldConfig = {};
        // this.delimittedFileConfig = {};
    }

    componentDidMount() {
        console.log("openProj::::", this.props.openProject)
        this.rappid = new RappidService(
            document.getElementById('app1'),
            new StencilService(),
            new ToolbarService(),
            new InspectorService(),
            new HaloService(),
            new KeyboardService()
        );
        this.rappid.startRappid();
        this.rappid.paper.on(
            "element:pointerdblclick",
            (cellView: joint.dia.CellView) => {
                console.log("type", cellView);

                if (cellView.model.attributes.elementType === "file")
                    this.setState({ visible: "four" });
                else if (cellView.model.attributes.elementType === "database")
                    this.setState({ visible: "five" });
                else if (
                    cellView.model.attributes.elementType === "recon")
                    this.setState({ visible: "one" });
                else if (cellView.model.attributes.type === 'standard.Rectangle' && cellView.model.attributes.attrs.label.text != "All")
                    this.setState({ isOpen: true });


                // this.getProjectInfo();

            });
        fetch(`api/Requests/GetWorkFlow/?id=${this.getUrlParameter("id")}`)
            .then(response => {
                if (response == undefined) return {};
                
                return response.json()
            })
            .then((responseJson) => {
                
                if (JSON.stringify(responseJson) != "{}") {
                    
                    this.rappid.graph.fromJSON(responseJson);
                }
            })

        fetch(`api/requests/GetRequestbyid/?id=${this.getUrlParameter("id")}`)
            .then(response => {
                if (response == undefined) return {};

                return response.json()
            })
            .then((responseJson) => {

                if (JSON.stringify(responseJson) != "{}") {
                    this.setState({ queryData: responseJson["properties"] });
                  //  alert(this.state.queryData[0]["name"]);
                    this.createDataSource();
                   // alert(JSON.stringify(responseJson["properties"][0]["name"]));
                }
            })

    }

    getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };

   
    createDataSource=()=> {
        //alert(JSON.stringify(this.state.queryData[0]["validations"][0]["rule"]));
       // var tmpdataSource = [];
       // for (var i = 0; i < this.state.queryData.length; i++) {
       //     tmpdataSource.push(this.state.queryData[i]["validations"]);
       // } 
       // alert(JSON.stringify(tmpdataSource));
       //var newtmpdataSource = JSON.stringify(tmpdataSource);
       // var newdataSource = newtmpdataSource.substring(1, newtmpdataSource.length - 1);
       // alert(newdataSource);

        //const tmpdataSource = [{
        //    key: '1',
        //    name: this.state.queryData[0]["name"],
        //    age: "" + this.state.queryData[0]["validations"][0]["rule"] +" "+this.state.queryData[0]["validations"][0]["value"],
        //    address: '10 Downing Street'
        //}, {
        //    key: '2',
        //        name: this.state.queryData[1]["name"],
        //        age: "" + this.state.queryData[0]["validations"][0]["rule"] + " " + this.state.queryData[0]["validations"][0]["value"],
        //    address: '10 Downing Street'
        //}
        //];

        const tmpdataSource = [{
  key: '1',
  name: 'Valid Card',
  age: 'Equals to 200',
  address: '10 Downing Street'
}, {
  key: '2',
  name: 'Valid Amount',
  age: 'Less Than Equals to 100',
  address: '10 Downing Street'
}
];

        this.setState({ dataSource: tmpdataSource });
    }

    //getProjectInfo() {

    //  this.setState({
    //    getReqObject: {},
    //  })

    //  console.log("FETCH STEP DATA ", this.state.getReqObject)

    //  fetch(urls.urlarray.getWorkFLowByStep + this.props.location.state.workFlowName + "/" +
    //    "reader" + "/" + this.props.location.state.workFlowType + "/" + this.props.openProject)
    //    .then(res => res.json())
    //    .then(
    //      (result) => {

    //        console.log("TEST RESULT :: ", result);

    //        this.setState({
    //          getReqObject: result,
    //        })

    //        localStorage.setItem("projectName", result.projectName);
    //        localStorage.setItem("stepName", "reader");
    //        localStorage.setItem("stepType", "fileReader");
    //        localStorage.setItem("workflowName", this.props.location.state.workFlowName);
    //        localStorage.setItem("workflowType", this.props.location.state.workFlowType);
    
    //      },

    //      (error) => {
    //        console.log("Cannot fetch product list");
    //        console.log(error);
    //      }
    //    )
    //}


    //componentWillReceiveProps(props) {
    //  console.log("Propssss::", props);

    //  this.uploadingFlowData(props);
    //  this.getProjectInfo();

    //}






    //uploadingFlowData(props) {


    //  if (props.location && props.location.state && props.location.state.workFlowType === "Recon") {
    //    console.log(urls.urlarray.getWorkFlow + props.openProject + "/" + props.location.state.workFlowName + "/" + "Recon")
    //    fetch(urls.urlarray.getWorkFlow + props.openProject + "/" + props.location.state.workFlowName + "/" + "Recon")
    //      .then(res => res.json())
    //      .then(
    //        (result) => {
    //          console.log("data_recon :: ", result);
    //          if (result) {
    //            if (result.flowData != null) {
    //              this.rappid.graph.fromJSON(result.flowData);
    //            } else {
    //              var filePath = require('./reconFile.json');
    //              this.rappid.graph.fromJSON(filePath);
    //            }

    //          } else {
    //            var filePath = require('./reconFile.json');
    //            this.rappid.graph.fromJSON(filePath);
    //          }
    //        },

    //        (error) => {
    //          var filePath = require('./reconFile.json');
    //          this.rappid.graph.fromJSON(filePath);
    //        }
    //      )
    //  } else {
    //    console.log(urls.urlarray.getWorkFlow + props.openProject + "/" + props.location.state.workFlowName + "/" + "ETL")
    //    fetch(urls.urlarray.getWorkFlow + props.openProject + "/" + props.location.state.workFlowName + "/" + "ETL")
    //      .then(res => res.json())
    //      .then(
    //        (result) => {
    //          console.log("data_flow :: ", result);
    //          if (result) {
    //            if (result.flowData != null) {
    //              this.rappid.graph.fromJSON(result.flowData);
    //            } else {
    //              var filePath = require('./jsonFile.json');
    //              this.rappid.graph.fromJSON(filePath);
    //            }

    //          } else {
    //            var filePath = require('./jsonFile.json');
    //            this.rappid.graph.fromJSON(filePath);
    //          }
    //        },

    //        (error) => {
    //          var filePath = require('./jsonFile.json');
    //          this.rappid.graph.fromJSON(filePath);
    //        }
    //      )
    //  }
    //}

    handleSave = () => {
        // alert(JSON.stringify(this.rappid.graph.toJSON()));

        fetch(`api/Requests/SaveWorkFlow`, {
            method: "POST",
            //  mode: 'no-cors',
            headers: {
                "Content-Type": "application/json"
                //   'Access-Control-Allow-Origin':'*'
            },
            body: JSON.stringify({
                "id": this.getUrlParameter("id"),
                "WorkFlowJson": JSON.stringify(this.rappid.graph),
            })
        })
            .then(response => {
                console.log("saved successfully", response);
            })
            .catch(err => err);
    }
    goBack  ()  {
       // this.props.history.goBack();
    }

    //setData(key, value) {

    //  this.reqObject.projectName = this.props.openProject;
    //  this.reqObject.stepName = "reader"; //localStorage.getItem("stepName");
    //  this.reqObject.stepType = "fileReader";//localStorage.getItem("stepType");
    //  this.reqObject.workflowName = this.props.location.state.workFlowName;
    //  this.reqObject.workflowType = this.props.location.state.workFlowType;



    //  if (key === 'fileformat') {
    //    this.stepConfig.sourceType = value;
    //    this.reqObject.stepConfig = this.stepConfig;
    //  }

    //  if (key === "charEncoding") {
    //    this.stepConfig.charEncoding = value;
    //    this.reqObject.stepConfig = this.stepConfig;
    //  }

    //  if (key === "fileLayout") {
    //    this.stepConfig.fileLayout = value;
    //    this.reqObject.stepConfig = this.stepConfig;
    //  }


    //  if (key === "recordExtractor") {
    //    this.stepConfig.recordExtractor = value;
    //    this.reqObject.stepConfig = this.stepConfig;
    //  }

    //  if (key === "recordDelimitter") {
    //    this.stepConfig.recordDelimitter = value;
    //    this.reqObject.stepConfig = this.stepConfig;
    //  }

    //  if (key === "recordLength") {
    //    this.stepConfig.recordLength = value;
    //    this.reqObject.stepConfig = this.stepConfig;
    //  }

    //  if(key === "fieldTokenType"){

    //    console.log("fieldTokenType ::: ")

    //    this.setState({
    //      tokenizerData : value,
    //    })


    //  }

    //  if (key === "fieldConfig") {
    //    this.fieldConfig = value;
    //    this.stepConfig.fieldConfig = this.fieldConfig;
    //    this.reqObject.stepConfig = this.stepConfig;
    //  }

    //  localStorage.setItem("requestObject", this.reqObject);
    //  console.log("REQ OBJECT ::", this.reqObject)
    //}


    //onApply(){

    //  console.log("Apply Called :: ", this.reqObject);

    //      fetch(urls.urlarray.saveWorkflowByStep, {
    //        mode: 'cors',
    //        headers: {
    //          'Accept': 'application/json',
    //          'Content-Type': 'application/json',
    //          'Access-Control-Allow-Origin': '*'
    //        },
    //        method: "POST",
    //        body: JSON.stringify(this.reqObject)
    //      })


    //        .then(function (response) {
    //          console.log("All Response - ", response);
    //          console.log(JSON.stringify(response));
    //          console.log("Response Status Code - ", response.status)

    //          if (response.status === 200) {
    //            console.log("Get successfull");
    //            message.success("Data Saved successfully");
    //            this.setState({ validate: true });
    //            this.setState({ error_msg: false });
    //            // console.log("After return", this.state.validate);
    //          }
    //          else if (response.status === 404) {
    //            //console.log("Username password do not match");
    //            this.setState({ validate: false });
    //            this.setState({ error_msg: true });
    //          }
    //       // this.handleCancel();
    //        }.bind(this))
    //        .catch(function (error) {
    //          console.log(error);
    //        });

    //}

    //saveWorkFlowData() {

    //  console.log("REQUEST OBJECT :: ", this.reqObject);

    //  fetch(urls.urlarray.saveWorkflowByStep, {
    //    mode: 'cors',
    //    headers: {
    //      'Accept': 'application/json',
    //      'Content-Type': 'application/json',
    //      'Access-Control-Allow-Origin': '*'
    //    },
    //    method: "POST",
    //    body: JSON.stringify(this.reqObject)
    //  })


    //    .then(function (response) {
    //      console.log("All Response - ", response);
    //      console.log(JSON.stringify(response));
    //      console.log("Response Status Code - ", response.status)

    //      if (response.status === 302) {
    //        console.log("Get successfull");
    //        this.setState({ validate: true });
    //        this.setState({ error_msg: false });
    //        // console.log("After return", this.state.validate);
    //      }
    //      else if (response.status === 404) {
    //        //console.log("Username password do not match");
    //        this.setState({ validate: false });
    //        this.setState({ error_msg: true });
    //      }
    //      this.handleCancel();
    //    }.bind(this))
    //    .catch(function (error) {
    //      console.log(error);
    //    });
    //}




    //handleUpload() {
    //  if (
    //    this.props.location &&
    //    this.props.location.state &&
    //    this.props.location.state.name === "recon"
    //  ) {
    //    var filePath = require("./jsonFileRecon.json");
    //    console.log(filePath);
    //    this.rappid.graph.fromJSON(filePath);
    //  } else {
    //    var filePath = require("./jsonFile.json");
    //    console.log(filePath);
    //    this.rappid.graph.fromJSON(filePath);
    //  }
    //}

    setCurrentTab = val => {
        this.currentTab = val;
    };

    showModal = () => {
        this.setState({
            visible: true
        });
    };
    showModal2 = () => {
        this.setState({
            visible: "four"
        });
    };
    showModal3 = () => {
        this.setState({
            addRecordVisible: true
        });
    };

    showModal5 = () => {
        this.setState({
            visible: "five"
        });
    };


    handleOk = () => {
        this.setState({
            visible: "two"
        });
    };
    handleOK1 = () => {
        this.setState({
            visible: "false"
        });
    };

    handleCancel = () => {
        // console.log("HANDLE CANCEL visible::: 1", this.state.visible)
        this.setState({ visible: false });
        // console.log("HANDLE CANCEL visible::: 2", this.state.visible)
    };
    handleModal2 = () => {
        this.setState({
            visible: "three"
        });
    };
    handleModal = () => {
        this.setState({
            visible: "false"
        });
    };

    //handleSaveTab = () => {
    //  const form = this.saveSourceForm;
    //  this.setState({ visible: false });
    //};
    handleCreate = () => {
        const form = this.formRef.props.form;

        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            console.log("Received values of form: ", values);
            this.setState({ visible: false });
            this.setState({ loadervisible: true });
            // this.setVal(values);
            form.resetFields();
            this.setState({ sourcedef: true });

            this.setState({ loadervisible: false });

        });
    };
    saveFormRef = formRef => {
        console.log("saveFormRef ::: ");
        this.formRef = formRef;
    };

    handleAddRecordCancel = () => {
        console.log("handleAddRecordCancel ::: ");
        this.setState({ addRecordVisible: false });
    };
    showCSVModal = () => {
        this.setState({
            isOpen: true,
        });
    }

    handleCSVOk = (e) => {

        this.setState({
            isOpen: false,
        });
    }

    handleCSVCancel = (e) => {

        this.setState({
            isOpen: false,
        });

    }



    //onUploadChange(info) {
    //  if (info.file.status !== 'uploading') {
    //    console.log(info.file, info.fileList);
    //  }
    //  if (info.file.status === 'done') {
    //    this.setState({ autoConfigDisable: false });
    //    message.success(`${info.file.name} file uploaded successfully you can now use auto Configuration`);
    //  } else if (info.file.status === 'error') {
    //    message.error(`${info.file.name} file upload failed.`);
    //  }
    //}

    render() {


        //const props = {
        //  name: 'file',
        //  action: '//jsonplaceholder.typicode.com/posts/',
        //  headers: {
        //    authorization: 'authorization-text',
        //  }, onChange: this.onUploadChange,
        //  multiple: false
        //};

        // console.log("HANDLE CANCEL visible::: 3", this.state.visible)
        const modalview = this.state.visible;
      //  alert("hi"+JSON.stringify(this.state.dataSource));

        return (
            <div id="app1" ref="app1" className="joint-app joint-theme-modern">
                <div className="app-header">
                    {/*  <Popover  placement="right"  title="Datasets"
            content={hoverContent.maparray.question_info}
          >
            <Icon  className="infoIcon"  type="question-circle"  style={{ marginTop:  "5px", cursor: 'pointer'  }}  />
          </Popover> */}
                    {/* <div className="app-title">
                        <h1>Optimus</h1>
                    </div> */}
                    {/* <div className="toolbar-container" /> */}
                </div>
                {modalview === "one" ? (
                    <div>
                        <ReconProcess1
                            visible={this.state.visible}
                            onOk={this.handleOk}
                            onCancel={this.handleCancel}
                        />
                    </div>
                ) : modalview === "two" ? (
                    <div>
                        <ReconProcess2
                            visible={this.state.visible}
                            onOk={this.handleModal2}
                            onCancel={this.handleCancel}
                        />
                    </div>
                ) : modalview === "three" ? (
                    <div>
                        <ReconProcess3
                            visible={this.state.visible}
                            onOk={this.handleCancel}
                            onCancel={this.handleCancel}
                        />
                    </div>
                ) : modalview === "five" ? (
                    <div>
                        <SourceLoader
                            visible={this.state.visible}
                            onOk={this.handleCancel}
                            onCancel={this.handleCancel}
                        />
                    </div>
                ) : modalview === "four" ? (<div>
                    <TabModal
                        // stepData={this.state.getReqObject}
                        //  setData={this.setData}
                        // setForm={this.saveSourceForm}
                        // activeTab={this.state.tokenizerData}
                        setCurrentTab={this.setCurrentTab}
                        onClick={this.showModal3}
                        visible={this.state.visible}
                        // onOk={this.saveWorkFlowData}
                        onCancel={this.handleCancel}
                    // onApply = {this.onApply} />
                    />

                    <AddNewRecord
                        wrappedComponentRef={this.saveFormRef}
                        visible={this.state.addRecordVisible}
                        onCancel={this.handleAddRecordCancel}
                        onCreate={this.handleCreate}
                        visible1={this.state.loadervisible} />
                </div>
                ) : (<div></div>)}

                <div className="app-body">
                    <div className="stencil-container" />
                    <div className="paper-container" />
                    {/*    <div className="inspector-container" /> */}
                </div>


                <div style={{ float: "right", paddingRight: "30px", marginTop: '100px'}}>
                    {/* <Link to={{ pathname: '/flowtype', state: { openP: this.props.openProject } }}>
          //  <Button>Back</Button>
          </Link>*/}
                    &nbsp;&nbsp;
          <Button variant="contained"
                        onClick={this.handleSave}
                        style={{
                            marginBottom: '100px',
                            fontSize: '12px',
                            background: '#1890ff',
                            color: 'white'
                        }}>Save</Button>
                     &nbsp; &nbsp;
                    <LinkContainer to={`/request/${this.getUrlParameter("id")}`}
                        style={{
                        marginBottom: '100px',
                        fontSize: '12px',
                        background: '#1890ff',
                        color: 'white'
                    }} >
                    <Button variant="contained"
                       
                        style={{
                            marginBottom: '100px',
                            fontSize: '12px',
                            background: '#1890ff',
                            color: 'white'
                            }}>Back</Button>
                        </LinkContainer>
                    {/* changed by Divyansh */}
                    {/*   <Link  to={{pathname: '/tablescreen',
            state: { workFlowName:  this.props.location.state.workFlowName ,workFlowType : this.props.location.state.workFlowType}}}> */}
                   
                    {/* </Link> */}

                    {/*  <Upload {...props}>
            <Button className="ant-btn btn ant-btn-primary" >Upload Sample file</Button>
          </Upload>
       */}



                    {/* till here */}

                    {/*  <Button
            className="ant-btn ant-btn-primary"
            onClick={this.handleUpload}
          >
            Upload
          </Button> */}
                </div>
                <div>
                    <CSVDialogBox isOpen={this.state.isOpen} dataSource={this.state.dataSource}  handleOk={this.handleCSVOk} handleCancel={this.handleCSVCancel} />
                </div>
      </div>
        );
    }
}

export default Rappid;
