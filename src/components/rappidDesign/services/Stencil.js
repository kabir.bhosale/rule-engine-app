import React from 'react';
import {ui, dia} from '../../exports/rappid';
//import {StencilService} from './stencil-service';
import joint from '../../exports/rappid';

export default class StencilSer {

    constructor(){
        this.stencil =  ui.Stencil;
        // this.paperScroller = joint.ui.PaperScroller;
        // this.snaplines = joint.ui.Snaplines;
        this.create();
        

    }

    create(){

        var graph = new joint.dia.Graph;
        var paper = new joint.dia.Paper({
                 el: $('#paper'),
                width: 500,
                height: 300,
                model: graph
            });

        var r = new joint.shapes.basic.Rect({
            position: { x: 10, y: 10 }, size: { width: 50, height: 30 }
        });
        var c = new joint.shapes.basic.Circle({
            position: { x: 70, y: 10 }, size: { width: 50, height: 30 }
        });

        var stencil = new joint.ui.Stencil({
            paper: paper,
            width: 200,
            height: 300
        });

        stencil.load([r, c]);
    }
    render(){
        return(
            <div >
                <div id = "paper"/>
            </div>
        )
    }
    // createStencil(){
    //     stencil = new ui.Stencil({
    //         paperScroller =  ui.PaperScroller,
    //         snaplines: snaplines,
    //         width: 150,
    //         dropAnimation: false,
    //         groupsToggleButtons: true,
    //         paperOptions: function () {
    //             return {
    //                 model: new dia.Graph({},{
    //                     cellNamespace: appShapes
    //                 }),
    //                 cellViewNamespace: appShapes
    //             };
    //         },
    //         layout: true,
    //         dragStartClone: cell => cell.clone().removeAttr('root/dataTooltip')

    //     });
    // }

    // setShapes() {
    //     this.stencil.load(this.getStencilShapes());
    // }
    
    // getStencilShapes() {
    //         standard: [
    //             {
    //                 type: 'standard.Rectangle',
    //                 size: { width: 1, height: 2 },
    //                 attrs: {
    //                     root: {
    //                         dataTooltip: 'Rectangle',
    //                         dataTooltipPosition: 'left',
    //                         dataTooltipPositionSelector: '.joint-stencil'
    //                     },
    //                     body: {
    //                         rx: 2,
    //                         ry: 2,
    //                         width: 50,
    //                         height: 30,
    //                         fill: 'transparent',
    //                         stroke: '#31d0c6',
    //                         strokeWidth: 2,
    //                         strokeDasharray: '0'
    //                     },
    //                     label: {
    //                         text: '',
    //                         fill: '#c6c7e2',
    //                         fontFamily: 'Roboto Condensed',
    //                         fontWeight: 'Normal',
    //                         fontSize: 11,
    //                         strokeWidth: 0
    //                     }
    //                 }
    //             }
    //         ]
    //     };
    
}