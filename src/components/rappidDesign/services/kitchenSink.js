import {ui, dia , joint } from '../../vendor/rappid';
import * as _ from 'lodash';
import {Stencil} from './Stencil';

class KitchenSink{

    constructor(){
        this.graph =  joint.dia.Graph;
        this.paper = joint.dia.Paper;
        this.paperScroller = joint.ui.PaperScroller;
        this.stencil = Stencil;
        this.el = Element;
    }
    startRappid() {
        this.initializeStencil();
    
    }

    initializeStencil() {

        this.stencil.create(this.paperScroller, this.snaplines);
        this.renderPlugin('.stencil-container', this.stencil.stencil);
        this.stencil.setShapes();
    }
}

