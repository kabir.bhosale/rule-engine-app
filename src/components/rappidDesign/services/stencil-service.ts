/*! Rappid v2.4.0 - HTML5 Diagramming Framework - TRIAL VERSION

Copyright (c) 2015 client IO

 2018-11-27 


This Source Code Form is subject to the terms of the Rappid Trial License
, v. 2.0. If a copy of the Rappid License was not distributed with this
file, You can obtain one at http://jointjs.com/license/rappid_v2.txt
 or from the Rappid archive as was distributed by client IO. See the LICENSE file.*/

import * as joint from '../../vendor/rappid';
import { ui, dia } from '../../vendor/rappid';
import * as appShapes from '../shapes/app-shapes';
import { element } from 'prop-types';
//import csvImage from '../../assets/csvImage.png';
export class StencilService {

    stencil: ui.Stencil;

    create(paperScroller: ui.PaperScroller, snaplines: ui.Snaplines) {
        this.stencil = new ui.Stencil({
            paper: paperScroller,
            snaplines: snaplines,
            width: 60,
            //groups: this.getStencilGroups(),
            dropAnimation: false,
            groupsToggleButtons: false,
            paperOptions: function () {
                return {
                    model: new dia.Graph({}, {
                        cellNamespace: appShapes
                    }),
                    cellViewNamespace: appShapes,
                };
            },
            // search: {
            //     '*': ['type', 'attrs/text/text', 'attrs/.label/text'],
            //     'org.Member': ['attrs/.rank/text', 'attrs/.name/text']
            // },
            // Use default Grid Layout
            layout: {
                columnWidth: 50,
                columns: 1,
                rowHeight: 40,
            },
            // Remove tooltip definition from clone
            dragStartClone: (cell) => cell.clone().removeAttr('root/dataTooltip'),
            scaleClones: true
        });
        console.log("Stencil Service", paperScroller);
    }

    setShapes() {
        //this.stencil.load(this.getStencilShapes());
        this.stencil.load(this.getStencilShapes());
    }

    getStencilGroups() {
        return <{ [key: string]: ui.Stencil.Group }>{
            standard: { index: 1, label: 'Standard shapes' }
            // fsa: { index: 2, label: 'State machine' },
            // pn: { index: 3, label: 'Petri nets' },
            // erd: { index: 4, label: 'Entity-relationship' },
            // uml: { index: 5, label: 'UML' },
            // org: { index: 6, label: 'ORG' }
        };
    }

    getStencilShapes() {
        return (
            [
                {
                    type: 'standard.Rectangle',
                    size: { width: 5, height: 3 },
                    attrs: {
                        root: {
                            dataTooltip: 'Task',
                            dataTooltipPosition: 'left',
                            dataTooltipPositionSelector: '.joint-stencil'
                        },
                        body: {
                            rx: 2,
                            ry: 2,
                            width: 50,
                            height: 30,
                            fill: 'white',
                            stroke: '#404040',
                            strokeWidth: 1,
                            strokeDasharray: '0'
                        },
                        label: {
                            text: '',
                            fill: '#00000',
                            fontFamily: 'Roboto Condensed',
                            fontWeight: 'Normal',
                            fontSize: 8,

                        }
                    },
                },
                {
                    type: 'standard.Polygon',
                    size: { width: 5, height: 3 },
                    attrs: {
                        root: {
                            dataTooltip: 'Decision',
                            dataTooltipPosition: 'left',
                            dataTooltipPositionSelector: '.joint-stencil'
                        },
                        body: {
                            refPoints: '50,0 100,50 50,100 0,50',
                            fill: 'transparent',
                            stroke: '#404040',
                            strokeWidth: 1,
                            strokeDasharray: '0'
                        },
                        label: {
                            text: '',
                            fill: '#00000',
                            fontFamily: 'Roboto Condensed',
                            fontWeight: 'Normal',
                            fontSize: 8,

                        }
                    },
                    elementType: 'Decision'

                }
                // new joint.shapes.bpmn.Gateway({
                //     attrs: {
                //         '.body': { fill: 'white', stroke: 'black',strokeWidth: 2  },
                //         root: {
                //             dataTooltip: 'Decision',
                //             dataTooltipPosition: 'left',
                //             dataTooltipPositionSelector: '.joint-stencil'
                //         }
                //     }
                // }),
                /* {
                    type: 'standard.Image',
                    size: { width: 4, height: 5 },
                    attrs: {
                        root: {
                            dataTooltip: 'Decision',
                            dataTooltipPosition: 'left',
                            dataTooltipPositionSelector: '.joint-stencil'
                        },
                        image: {
                            xlinkHref: 'TaskDesignIcon/Decision.png'
                        },
                        body: {
                            fill: 'transparent',
                            stroke: '##404040',
                            strokeWidth: 1,
                            strokeDasharray: '0'
                        },
                        label: {
                            text: '',
                            fontFamily: 'Roboto Condensed',
                            fontWeight: 'Normal',
                            fontSize: 8,
                            fill: '#000'
                        },
                    },
                    elementType: "decision"
                } */,
                {
                    type: 'standard.Image',
                    size: { width: 4, height: 5 },
                    attrs: {
                        root: {
                            dataTooltip: 'Database',
                            dataTooltipPosition: 'left',
                            dataTooltipPositionSelector: '.joint-stencil'
                        },
                        image: {
                            xlinkHref: 'TaskDesignIcon/iconfinder_database_1608662.png'
                        },
                        body: {
                            fill: 'transparent',
                            stroke: '##404040',
                            strokeWidth: 1,
                            strokeDasharray: '0'
                        },
                        label: {
                            text: '',
                            fill: '#00000',
                            fontFamily: 'Roboto Condensed',
                            fontWeight: 'Normal',
                            fontSize: 8,
                        },
                    },
                    elementType: "database"
                }, {
                    type: 'standard.Image',
                    size: { width: 4, height: 5 },
                    attrs: {
                        root: {
                            dataTooltip: 'File',
                            dataTooltipPosition: 'left',
                            dataTooltipPositionSelector: '.joint-stencil'
                        },
                        image: {
                            xlinkHref: 'TaskDesignIcon/iconfinder_File_10485.png'
                        },
                        body: {
                            fill: 'transparent',
                            stroke: '##404040',
                            strokeWidth: 1,
                            strokeDasharray: '0'
                        },
                        label: {
                            text: '',
                            fontFamily: 'Roboto Condensed',
                            fontWeight: 'Normal',
                            fontSize: 8,
                            fill: '#000'
                        }
                    },
                    elementType: "file"
                },  
                /* {
                    type: 'standard.Image',
                    size: { width: 4, height: 5 },
                    attrs: {
                        root: {
                            dataTooltip: 'View',
                            dataTooltipPosition: 'left',
                            dataTooltipPositionSelector: '.joint-stencil'
                        },
                        image: {
                            xlinkHref: 'TaskDesignIcon/icons8-view-100.png'
                        },
                        body: {
                            fill: 'transparent',
                            stroke: '##404040',
                            strokeWidth: 1,
                            strokeDasharray: '0'
                        },
                        label: {
                            text: '',
                            fontFamily: 'Roboto Condensed',
                            fontWeight: 'Normal',
                            fontSize: 8,
                            fill: '#000'
                        }
                    },
                    elementType: "view"
                }, */
                {
                    type: 'fsa.StartState',
                    preserveAspectRatio: true,
                    attrs: {
                        root: {
                            dataTooltip: 'Start State',
                            dataTooltipPosition: 'left',
                            dataTooltipPositionSelector: '.joint-stencil'
                        },
                        circle: {
                            width: 50,
                            height: 30,
                            fill: '#404040',
                            'stroke-width': 0
                        },
                        text: {
                            text: '',
                            fontFamily: 'Roboto Condensed',
                            fontWeight: 'Normal',
                            fontSize: 8,
                            fill: '#000'
                        }
                    }
                },

                /*  {
                     type: 'standard.Image',
                     attrs: {
                         root: {
                             dataTooltip: 'End State',
                             dataTooltipPosition: 'left',
                             dataTooltipPositionSelector: '.joint-stencil'
                         },
                         image: {
                             xlinkHref: 'TaskDesignIcon/End.png'
                         },
                         body: {
                             fill: 'transparent',
                             stroke: '##404040',
                             strokeWidth: 1,
                             strokeDasharray: '0'
                         },
                         label: {
                             text: '',
                             fontFamily: 'Roboto Condensed',
                             fontWeight: 'Normal',
                             fontSize: 8,
                             fill: '#000'
                         }
                     },
                     elementType: "endState"
                 }, */

                {
                    type:  'fsa.EndState',
                    preserveAspectRatio:  true,
                    attrs:  {
                        root:  {
                            dataTooltip:  'End State',
                            dataTooltipPosition:  'left',
                            dataTooltipPositionSelector:  '.joint-stencil'
                        },
                        '.inner':  {
                            fill:  '#404040',
                            stroke:  'transparent'
                        },
                        '.outer':  {
                            fill:  'transparent',
                            stroke:  '#404040',
                            'stroke-width':  2,
                            'stroke-dasharray':  '0'
                        },
                        text:  {
                            text: '',
                            fontFamily: 'Roboto Condensed',
                            fontWeight: 'Normal',
                            fontSize: 8,
                            fill: '#000'
                        }
                    }
                } ,

                new joint.shapes.bpmn.Event({
                    attrs: {
                        '.body': { fill: 'white', stroke: 'black' },
                        '.label': {text: '',
                        fontFamily: 'Roboto Condensed',
                        fontWeight: 'Normal',
                        fontSize: 8,
                        fill: '#000'},
                        root: {
                            dataTooltip: 'Event',
                            dataTooltipPosition: 'left',
                            dataTooltipPositionSelector: '.joint-stencil'
                        },
                        label: {
                            text: '',
                            fontFamily: 'Roboto Condensed',
                            fontWeight: 'Normal',
                            fontSize: 8,
                            fill: '#000'
                        }
                    }
                }),

                new joint.shapes.bpmn.Message({
                    attrs: {
                        '.body': { fill: 'white', stroke: 'black' },
                        '.label': {text: '',
                        fontFamily: 'Roboto Condensed',
                        fontWeight: 'Normal',
                        fontSize: 8,
                        fill: '#000'},
                        root: {
                            dataTooltip: 'Event',
                            dataTooltipPosition: 'left',
                            dataTooltipPositionSelector: '.joint-stencil'
                        },
                        label: {
                            text: '',
                            fontFamily: 'Roboto Condensed',
                            fontWeight: 'Normal',
                            fontSize: 1,
                            fill: '#000'
                        }
                    }
                }),

               /*  {
                    type: 'standard.Image',
                    attrs: {
                        root: {
                            dataTooltip: 'Message',
                            dataTooltipPosition: 'left',
                            dataTooltipPositionSelector: '.joint-stencil'
                        },
                        image: {
                            xlinkHref: 'TaskDesignIcon/Message.png'
                        },
                        body: {
                            fill: 'transparent',
                            stroke: '##404040',
                            strokeWidth: 1,
                            strokeDasharray: '0'
                        },
                        label: {
                            text: '',
                            fontFamily: 'Roboto Condensed',
                            fontWeight: 'Normal',
                            fontSize: 8,
                            fill: '#000'
                        }
                    },
                    elementType: "message"
                }, */
                {
                    type: 'standard.Image',
                    size: { width: 4, height: 5 },
                    attrs: {
                        root: {
                            dataTooltip: 'Brief',
                            dataTooltipPosition: 'left',
                            dataTooltipPositionSelector: '.joint-stencil'
                        },
                        image: {
                            xlinkHref: 'TaskDesignIcon/iconfinder_017_Bag_183518.png'
                        },
                        body: {
                            fill: 'transparent',
                            stroke: '##404040',
                            strokeWidth: 1,
                            strokeDasharray: '0'
                        },
                        label: {
                            text: '',
                            fontFamily: 'Roboto Condensed',
                            fontWeight: 'Normal',
                            fontSize: 8,
                            fill: '#000'
                        }
                    },
                    elementType: "briefcase"
                },
                {
                    type: 'standard.Image',
                    size: { width: 4, height: 5 },
                    attrs: {
                        root: {
                            dataTooltip: 'Settings',
                            dataTooltipPosition: 'left',
                            dataTooltipPositionSelector: '.joint-stencil'
                        },
                        image: {
                            xlinkHref: 'TaskDesignIcon/iconfinder_setting-gear-process-engineering_2075815.png'
                        },
                        body: {
                            fill: 'transparent',
                            stroke: '##404040',
                            strokeWidth: 1,
                            strokeDasharray: '0'
                        },
                        label: {
                            text: '',
                            fontFamily: 'Roboto Condensed',
                            fontWeight: 'Normal',
                            fontSize: 8,
                            fill: '#000'
                        }
                    },
                    elementType: "settings"
                },

            ]);
    }
}
