/*! Rappid v2.4.0 - HTML5 Diagramming Framework - TRIAL VERSION

Copyright (c) 2015 client IO

 2018-11-27 


This Source Code Form is subject to the terms of the Rappid Trial License
, v. 2.0. If a copy of the Rappid License was not distributed with this
file, You can obtain one at http://jointjs.com/license/rappid_v2.txt
 or from the Rappid archive as was distributed by client IO. See the LICENSE file.*/


import * as joint from '../../vendor/rappid';
import { memoize } from 'lodash';

export namespace app {

    export class Rectangle extends joint.shapes.standard.Rectangle {

        defaults() {

            return joint.util.defaultsDeep({
                type: 'Rectangle',
                attrs: {
                    root: {
                        magnet: false
                    },
                    size : {
                        width : 400 ,
                        height: 350
                    },
                    label : {
                        fill : 'black'
                    },
                    style:{
                        width : 400 ,
                        height: 350
                    }
                },
            }, joint.shapes.standard.Rectangle.prototype.defaults);
        }

        defaultLabel = {
            attrs: {
                rect: {
                    fill: '#ffffff',
                    stroke: '#8f8f8f',
                    strokeWidth: 2,
                    refWidth: 10,
                    refHeight: 10,
                    refX: 0,
                    refY: 0,
                    width : 100,
                    height : 200
                }
            }
        };
    }

   
    export class Link2 extends joint.shapes.standard.Link  {

        defaults() {
            return joint.util.defaultsDeep({
                router: {
                    name: 'normal',
                },
                connector: {
                    name: 'normal',
                    
                },
                labels: [],
                attrs: {
                    line: {
                        stroke: 'black',
                        strokeDasharray: '0',
                        strokeWidth: 1,
                        fill: 'none',
                        sourceMarker: {
                            d: 'M 0 0 0 0',
                            fill: {
                                type: 'color-palette',
                                group: 'marker-source',
                                when: { ne: { 'attrs/line/sourceMarker/d': 'M 0 0 0 0' } },
                                index: 2
                            },
                            
                        },
                        targetMarker: {
                            d: ' M 0 -3 -6 0 0 3 z',
                            },
                           
                        }
                }
            }, joint.shapes.standard.Link.prototype.defaults);
        }
        
        defaultLabel = {
            attrs: {
                rect: {
                    fill: '#ffffff',
                    stroke: '#8f8f8f',
                    strokeWidth: 2,
                    refWidth: 10,
                    refHeight: 10,
                    refX: 0,
                    refY: 0
                }
            }
        };

        
        
        getMarkerWidth(type: any) {
            const d = (type === 'source') ? this.attr('line/sourceMarker/d') : this.attr('line/targetMarker/d');
            console.log("marker width", this.getDataWidth(d));
            return this.getDataWidth(d);
        }

        getDataWidth = memoize(function (d: any) {
            return (new joint.g.Rect(d)).bbox().width;
        });

        static connectionPoint(line: any, view: any, magnet: any, opt: any, type: any, linkView: any) {
            // const markerWidth = linkView.model.getMarkerWidth(type);
            opt = { offset: 5, stroke: true };
            // connection point for UML shapes lies on the root group containg all the shapes components
            const modelType = view.model.get('type');
            if (modelType.indexOf('uml') === 0) opt.selector = 'root';
            // taking the border stroke-width into account
            if (modelType === 'standard.InscribedImage') opt.selector = 'border';
            return joint.connectionPoints.boundary.call(this, line, view, magnet, opt, type, linkView);
        }
    }
    export class Link1 extends joint.shapes.standard.DoubleLink {

        defaults(){
            return joint.util.defaultsDeep({
                // source: { x: 10, y: 200 },
                // target: { x: 350, y: 200 },
                attrs: {
                    line: {
                        stroke: '#7c68fc',
                        cursor: 'pointer',
                        strokeDasharray: '0',
                        strokeWidth: 10,
                        fill: 'none',
                        sourceMarker: {
                            type: 'path',
                            d: 'M 0 0 0 0',
                            stroke: 'none',
                           
                        },
                        targetMarker: {
                            type: 'path',
                            d: 'M 0 0 0 0',
                            stroke: 'none'
                        }
                    }
                }
            
                
    
            });
        }
        
        // tools = [
        //     new joint.linkTools.Vertices(),
        //             new CustomBoundary({ padding: 25 })
        // ];
    }
}


// var CustomBoundary = joint.linkTools.Boundary.extend({
//     attributes: {
//         'fill': '#7c68fc',
//         'fill-opacity': 0.2,
//         'stroke': '#33334F',
//         'stroke-width': .5,
//         'stroke-dasharray': '5, 5',
//         'pointer-events': 'none'
//     },
// });
export const NavigatorElementView = joint.dia.ElementView.extend({

    body: null,

    markup: [{
        tagName: 'rect',
        selector: 'body',
        attributes: {
            'fill': '#31d0c6'
        }
    }],

    initialize() {
        this.listenTo(this.model, 'change:position', this.translate);
        this.listenTo(this.model, 'change:size', this.resize);
        this.listenTo(this.model, 'change:angle', this.rotate);
    },

    render() {
        const doc = joint.util.parseDOMJSON(this.markup);
        this.body = doc.selectors.body;
        this.el.appendChild(doc.fragment);
        this.update();
    },

    update() {
        const size = this.model.get('size');
        this.body.setAttribute('width', size.width);
        this.body.setAttribute('height', size.height);
        this.updateTransformation();
    }
});

export const NavigatorLinkView = joint.dia.LinkView.extend({

    initialize: joint.util.noop,

    render: joint.util.noop,

    update: joint.util.noop
});

// re-export build-in shapes from rappid
export const basic = joint.shapes.basic;
export const standard = joint.shapes.standard;
export const fsa = joint.shapes.fsa;
export const pn = joint.shapes.pn;
export const erd = joint.shapes.erd;
export const uml = joint.shapes.uml;
export const org = joint.shapes.org;

