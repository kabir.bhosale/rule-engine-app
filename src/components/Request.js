﻿import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Home } from "./Home";
import { actionCreators } from "../store/Request";
import { AppInfo } from "./ChildComponents/apiInfo";
import { PropertyBar } from "./ChildComponents/propertybar";
// import Button from "@material-ui/core/Button";
import { Button } from "antd";
import { LinkContainer } from "react-router-bootstrap";
import "./ChildComponents/react-treeview.css";

var Modal_button_style = {
  margin: "10px",
  fontSize: "12px"
};
var Color_button_style = {
  margin: "10px",
  fontSize: "12px",
  background: "#1890ff",
  color: "white"
};
class Request extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      errors: {}
    };
  }

  handleSave = () => {
    var date = Date();

    var iserror = false;
    let errors = {};
    var name = document.getElementById("name");

    var method = document.getElementById("method");

    var path = document.getElementById("path");

    var summary = document.getElementById("summary");
    var type = document.getElementById("type");
    var port = document.getElementById("port");

    if (name.value == "") {
      name.classList.add("error");

      iserror = true;
      errors["name"] = "Cannot be empty";
    } else {
      name.classList.remove("error");

      iserror = false;
    }

    if (method.value == "") {
      method.classList.add("error");

      iserror = true;
      errors["method"] = "Cannot be empty";
    } else {
      method.classList.remove("error");

      iserror = false;
    }

    if (path.value == "") {
      path.classList.add("error");

      iserror = true;
      errors["path"] = "Cannot be empty";
    } else {
      path.classList.remove("error");

      iserror = false;
    }

    if (type.value == "") {
      type.classList.add("error");

      iserror = true;
      errors["type"] = "Cannot be empty";
    } else {
      type.classList.remove("error");

      iserror = false;
    }

    if (port.value == "") {
      port.classList.add("error");

      iserror = true;
      errors["port"] = "Cannot be empty";
    } else {
      port.classList.remove("error");

      iserror = false;
    }

    if (summary.value == "" || summary.value == null) {
      summary.classList.add("error");

      iserror = true;
      errors["summary"] = "Cannot be empty";
    } else {
      summary.classList.remove("error");

      iserror = false;
    }

    if (iserror) return;
    //alert(JSON.stringify(this.props.requestResult));
    // let url=`api/requests/`;
    let url = `http://10.10.18.13:6858/api/requests/`;
    fetch(`${url}?date=${JSON.stringify(this.props.requestResult)}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        id: this.props.requestResult.id,
        json: JSON.stringify(this.props.requestResult)
      })
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          data: responseJson.token
        });

        // alert("Data das been saved");
        window.location = window.location;
      });
  };

  handleClose = () => {
    //alert(JSON.stringify(this.prop.requestResult));
  };

  componentWillMount() {
    const id = parseInt(this.props.match.params.id, 10) || 0;
    this.props.getRequest(id);
  }

  componentWillReceiveProps(nextProps) {
    // This method runs when incoming props (e.g., route params) change
    const id = parseInt(nextProps.match.params.id, 10) || 0;
    this.props.getRequest(id);
  }

  render() {
    return (
      <div>
        <div>
          {/* <h2
            className="col-sm-6"
            style={{ marginTop: "15px" }}
            title="This UI Provide you interface to create/edit a new request which can be deployed as Api or exported as .jar file"
          >
            Create API
          </h2> 
          <div className="col-sm-6" style={{ float: "right", marginTop: 15 }}>
              <Button
                variant="contained"
                onClick={this.handleClose}
                style={Modal_button_style}
              >
                Cancel
              </Button>
              <Button
                variant="contained"
                onClick={this.handleSave}
                style={Color_button_style}
              >
                Save
              </Button>

              <LinkContainer
                to={`/workflow?id=${this.props.requestResult.id}`}
                style={Color_button_style}
              >
                <Button
                  variant="contained"
                  color="primary"
                  style={Color_button_style}
                >
                  Manage WorkFlow
                </Button>
              </LinkContainer>
            </div>*/}
          <div className="header">
            <h2 style={{ padding: 12, display: "inline-block", marginTop: 8 }}>
              Create new API
            </h2>

            <div style={{ float: "right", marginTop: 14 }}>
              <Button type="danger" onClick={this.handleClose}>
                Cancel
              </Button>
              &nbsp;&nbsp;&nbsp;
              <Button type="primary" onClick={this.handleSave}>
                Save
              </Button>
              <LinkContainer
                to={`/workflow?id=${this.props.requestResult.id}`}
                style={Color_button_style}
              >
                <Button type="ghost">Manage WorkFlow</Button>
              </LinkContainer>
            </div>
          </div>
        </div>

        {renderRequestUI(this.props)}
      </div>
    );
  }
}

function renderRequestUI(props) {
  return (
    <div>
      <div className="main_container">
        <div role="main">
          <div className="col-md-12 col-sm-12 col-xs-12">
            <div className="row">
              <div className="col-md-12 col-sm-12 col-xs-12">
                <AppInfo {...props} />
              </div>
              <div className="ln_solid" />
              <div className="x_panel">
                <div className="x_title">
                  <br />
                  {/* <h3>Properties </h3> */}
                  <hr />
                  <div className="clearfix" />
                </div>
              </div>

              <div className="col-md-12 col-sm-12 col-xs-12">
                <PropertyBar {...props} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default connect(
  state => state.requests,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(Request);
