import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import { actionCreators } from "../store/WeatherForecasts";
import { Table } from "antd";

class FetchData extends Component {
  componentWillMount() {
    // This method runs when the component is first added to the page
    const startDateIndex =
      parseInt(this.props.match.params.startDateIndex, 10) || 0;
    this.props.requestWeatherForecasts(startDateIndex);
  }

  componentWillReceiveProps(nextProps) {
    // This method runs when incoming props (e.g., route params) change
    const startDateIndex =
      parseInt(nextProps.match.params.startDateIndex, 10) || 0;
    this.props.requestWeatherForecasts(startDateIndex);
  }

  render() {
    return (
      <div>
        <div className="header">
          <h2 style={{ padding: 24 }}>List of APIs</h2>
        </div>
        {/* <h1>APIs</h1> */}
        {/* <p>List of all the APIs.</p> */}
        {renderForecastsTable(this.props)}
        {/* {renderPagination(this.props)} */}
      </div>
    );
  }
}

function renderForecastsTable(props) {
  const columns = [
    {
      title: "ID",
      dataIndex: "id"
    },
    {
      title: "API Name",
      dataIndex: "name"
    },
    {
      title: "Summary",
      dataIndex: "summary"
    },
    {
      title: "Methods",
      dataIndex: "method",
      render: method => {
        if (method == 1) {
          return <div className="method post">POST</div>;
        } else if (method == 2) {
          return <div className="method put">PUT</div>;
        } else if (method == 3) {
          return <div className="method get">GET</div>;
        } else if (method == 4) {
          return <div className="method patch">PATCH</div>;
        } else {
          return <div className="method get">GET</div>;
        }
      }
    },
    {
      title: "API Url",
      dataIndex: "url"
    }
  ];

  return (
    <Table
      onRow={record => ({
        onClick: () => {
          //   location.href = "request/" + record.id;
        }
      })}
      rowKey={record => record.id}
      columns={columns}
      dataSource={props.forecasts}
      bordered
      //   title={() => "List of all APIs"}
      //   footer={() => "Footer"}
    />
    // <table className="table">
    //   <thead>
    //     <tr>
    //       <th>Id</th>
    //       <th>Name</th>
    //       <th>Summary</th>
    //       <th>Methods</th>
    //       <th>URL</th>
    //     </tr>
    //   </thead>
    //   <tbody>
    //     {props.forecasts.map(request => (
    //       <tr key={request.id}>
    //         <td>
    //           <a href={"/request/" + request.id}> {request.id}</a>
    //         </td>
    //         <td>{request.name}</td>
    //         <td>{request.summary}</td>
    //         {request.method === 1 ? (
    //           <td>POST</td>
    //         ) : request.method === 2 ? (
    //           <td>PUT</td>
    //         ) : request.method === 3 ? (
    //           <td>GET</td>
    //         ) : request.method === 4 ? (
    //           <td>PATCH</td>
    //         ) : (
    //           <td />
    //         )}

    //         <td>{request.url}</td>
    //       </tr>
    //     ))}
    //   </tbody>
    // </table>
  );
}

function renderPagination(props) {
  const prevStartDateIndex = (props.startDateIndex || 0) - 5;
  const nextStartDateIndex = (props.startDateIndex || 0) + 5;

  return (
    <p className="clearfix text-center">
      <Link
        className="btn btn-default pull-left"
        hidden="true"
        to={`/fetchdata/${prevStartDateIndex}`}
      >
        Previous
      </Link>
      <Link
        className="btn btn-default pull-right"
        hidden="true"
        to={`/fetchdata/${nextStartDateIndex}`}
      >
        Next
      </Link>
      {/* {props.isLoading ? <span>Loading...</span> : []} */}
    </p>
  );
}

export default connect(
  state => state.weatherForecasts,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(FetchData);
